//------------------------------------------------------------------------
//
//  Level Converter (C) 2017 Andrew Apted
//
//  This code is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 2
//  of the License, or (at your option) any later version.
//
//  This code is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <assert.h>

#include <vector>


typedef unsigned char byte;

typedef unsigned short u16_t;
typedef unsigned int   u32_t;

typedef short s16_t;
typedef int   s32_t;


// FIXME : these endian conversions assume x86
#define LE_U16(X)  ((u16_t)(X))
#define LE_U32(X)  ((u32_t)(X))

#define LE_S16(X)  ((s16_t) LE_U16((u16_t) (X)))
#define LE_S32(X)  ((s32_t) LE_U32((u32_t) (X)))


// --- on-disk structures ---

#define PACKEDATTR __attribute__((packed))

typedef struct
{
	char type[4];

	u32_t num_entries;
	u32_t dir_start;

} PACKEDATTR raw_wad_header_t;


typedef struct
{
	u32_t start;
	u32_t length;

	char name[8];

} PACKEDATTR raw_wad_entry_t;


typedef struct
{
	s16_t tid;       // thing tag id (for scripts/specials)
	s16_t x, y;      // position
	s16_t height;    // start height above floor
	s16_t angle;     // angle thing faces
	u16_t type;      // type of thing
	u16_t options;   // when appears, deaf, dormant, etc..

	byte special;    // special type
	byte arg[5];     // special arguments

}  PACKEDATTR raw_hexen_thing_t;



// directory entry

class lump_c
{
public:
	// name of lump
	char name[10];

	// offset to start of lump
	int start;

	// length of lump
	int length;

	// various flags
	int flags;

 	// # of following lumps (if a level), otherwise zero
	int children;

public:
	lump_c(const char *_name, int _start, int _len) :
		start(_start), length(_len),
		flags(0), children(0)
	{
		strcpy(name, _name);
	}

	~lump_c()
	{ }
};


// wad header

class wad_c
{
private:
	FILE *fp;

	// directory entries
	std::vector<lump_c *> lumps;

	// current data from ReadLump()
	byte * data_block;
	int    data_len;

public:
	wad_c() : fp(NULL), lumps(), data_block(NULL), data_len()
	{ }

	virtual ~wad_c();

	// open the wad file for reading and load the directory.
	// returns NULL on error (and an error message will have been set).
	// just delete the wad to close it.
	static wad_c * Open(const char *filename);

	// find a particular level in the directory and return its
	// index number, or -1 if it cannot be found.
	// use "*" as the name to find the first level.
	int FindLevel(const char *name);

	// read lump contents into memory.  only one lump can be read
	// at a time -- the wad code takes care of allocation / freeing.
	//
	// the 'length' variable will be set to the lump's length.
	// when 'level' is >= 0, the lump is part of a particular level.
	// returns NULL if the lump cannot be found.
	byte * ReadLump(const char *name, int *length, int level = -1);

private:
	bool ReadDirectory();
	bool ReadDirEntry();
	void DetermineLevels();

	int FindLump(const char *name, int level = -1);
	byte * AllocateData(int length);
	void FreeData();
};


wad_c * the_wad;


const char *level_lumps[] =
{
	"THINGS", "LINEDEFS", "SIDEDEFS", "VERTEXES", "SEGS",
	"SSECTORS", "NODES", "SECTORS", "REJECT", "BLOCKMAP",
	"BEHAVIOR",

	NULL  // end of list
};


#define Debug_Printf(...)  do {} while(0)


void FatalError(const char *msg, ...)
{
	static char buffer[2000];

	va_list arg_pt;

	va_start(arg_pt, msg);
	vsnprintf(buffer, 2000-1, msg, arg_pt);
	va_end(arg_pt);

	buffer[2000-2] = 0;

	fprintf(stderr, "ERROR : %s\n", buffer);

	if (the_wad)
	{
		delete the_wad;
		the_wad = NULL;
	}

	exit(9);
}



int CheckMagic(const char type[4])
{
	if ((type[0] == 'I' || type[0] == 'P') &&
		 type[1] == 'W' && type[2] == 'A' && type[3] == 'D')
	{
		return true;
	}

	return false;
}


int CheckLevelLump(const char *name)
{
	for (int i = 0 ; level_lumps[i] ; i++)
		if (strcmp(name, level_lumps[i]) == 0)
			return 1 + i;

	return 0;
}


wad_c::~wad_c()
{
	if (fp)
	{
		fclose(fp);
	}

	FreeData();

	for (unsigned int i = 0 ; i < lumps.size() ; i++)
		delete lumps[i];
}


byte * wad_c::AllocateData(int length)
{
	if (data_block && length <= data_len)
		return data_block;

	FreeData();

	data_len = length;
	data_block = new byte[length + 1];

	return data_block;
}


void wad_c::FreeData()
{
	if (data_block)
		delete[] data_block;

	data_block = NULL;
	data_len   = -1;
}


bool wad_c::ReadDirEntry()
{
	raw_wad_entry_t entry;

	size_t len = fread(&entry, sizeof(entry), 1, fp);
	if (len != 1)
	{
		FatalError("Trouble reading wad directory --> %s", strerror(errno));
		return false;
	}

	int start  = LE_U32(entry.start);
	int length = LE_U32(entry.length);

	// ensure name gets NUL terminated
	char name_buf[10];
	memset(name_buf, 0, sizeof(name_buf));
	memcpy(name_buf, entry.name, 8);

	lump_c *lump = new lump_c(name_buf, start, length);

#if DEBUG_WAD
	Debug_Printf("Read dir... %s\n", lump->name);
#endif

	lumps.push_back(lump);

	return true;  // OK
}


bool wad_c::ReadDirectory()
{
	raw_wad_header_t header;

	size_t len = fread(&header, sizeof(header), 1, fp);
	if (len != 1)
	{
		FatalError("Error reading wad header --> %s", strerror(errno));
		return false;
	}

	if (! CheckMagic(header.type))
	{
		FatalError("File is not a WAD file.");
		return false;
	}

	int num_entries = LE_U32(header.num_entries);
	int dir_start   = LE_U32(header.dir_start);

	Debug_Printf("Reading %d dir entries at 0x%X\n", num_entries, dir_start);

	fseek(fp, dir_start, SEEK_SET);

	for (int i = 0 ; i < num_entries ; i++)
	{
		if (! ReadDirEntry())
			return false;
	}

	return true;  // OK
}


void wad_c::DetermineLevels()
{
	for (unsigned int k = 0 ; k < lumps.size() ; k++)
	{
		lump_c * L = lumps[k];

		// skip known lumps (these are never valid level names)
		if (CheckLevelLump(L->name))
			continue;

		// check if the next four lumps after the current lump match the
		// level-lump names.  Order doesn't matter, but repeats do.
		int matched = 0;

		for (unsigned int i = 1 ; i <= 4 ; i++)
		{
			if (k + i >= lumps.size())
				break;

			lump_c * N = lumps[k + i];

			int idx = CheckLevelLump(N->name);

			if (! idx || idx > 8 /* SECTORS */ || (matched & (1<<idx)))
				break;

			matched |= (1<<idx);
		}

		if ((matched & 0xF) == 0xF)
			continue;

#if DEBUG_WAD
		Debug_Printf("Found level name: %s\n", L->name);
#endif

		// collect the children lumps
		L->children = 4;

		for (unsigned int j = 5 ; j < 16 ; j++)
		{
			if (k + j >= lumps.size())
				break;

			lump_c * N = lumps[k + j];

			if (! CheckLevelLump(N->name))
				break;

			L->children = j;
		}
	}
}


wad_c * wad_c::Open(const char *filename)
{
	FILE *in_file = fopen(filename, "rb");

	if (! in_file)
	{
		FatalError("Cannot open WAD file: %s --> %s", filename, strerror(errno));
		return NULL;
	}

	Debug_Printf("Opened WAD file : %s\n", filename);

	wad_c * wad = new wad_c();

	wad->fp = in_file;

	if (! wad->ReadDirectory())
	{
		delete wad;

		return NULL;
	}

//!!!	wad->DetermineLevels();

	return wad;
}


int wad_c::FindLump(const char *name, int level)
{
	int first = (level < 0) ? 0 : level + 1;
	int last  = (level < 0) ? (int)lumps.size() - 1 : level + lumps[level]->children;

	for (int i = first ; i <= last ; i++)
	{
		lump_c * L = lumps[i];

		if (strcmp(L->name, name) == 0 && L->children == 0)
			return i;
	}

	return -1;  // NOT FOUND
}


int wad_c::FindLevel(const char *name)
{
	for (int i = 0 ; i < (int)lumps.size() ; i++)
	{
		lump_c * L = lumps[i];

		if (L->children == 0)
			continue;

		if (name[0] == '*' || (strcmp(L->name, name) == 0))
			return i;
	}

	return -1;  // NOT FOUND
}


byte * wad_c::ReadLump(const char *name, int *length, int level)
{
	int index = FindLump(name, level);

	if (index < 0)
	{
		FatalError("Missing %slump: '%s'", level ? "level " : "", name);
		return NULL;
	}

	lump_c * L = lumps[index];

#if DEBUG_WAD
	Debug_Printf("Reading lump: %s (%d bytes)\n", L->name, L->length);
#endif

	if (length)
		(*length) = L->length;

	byte * data = AllocateData(L->length);

	if (L->length > 0)
	{
		fseek(fp, L->start, SEEK_SET);

		size_t len = fread(data, L->length, 1, fp);
		if (len != 1)
		{
			FatalError("Trouble reading lump '%s' --> %s", name, strerror(errno));
			return NULL;
		}
	}

	return data;
}


//------------------------------------------------------------------------


const char *CalcFlagsString(int flags)
{
	return "";
}


const char *MakePadding(const char *name)
{
	int len = 10 - (int)strlen(name);

	if (len <= 0)
		return "";

	static char buffer[64];
	char *p = buffer;

	for ( ; len > 0 ; len--)
		*p++ = ' ';

	*p = 0;

	return buffer;
}


void VisitThing(raw_hexen_thing_t *raw)
{
	double x = LE_S16(raw->x);
	double y = LE_S16(raw->y);
	double z = LE_S16(raw->height);

	int ed_num = LE_U16(raw->type);
	int flags  = LE_U16(raw->options);

	int angle = LE_S16(raw->angle);
	int tid   = LE_S16(raw->tid);

	printf("    { ed_num=%4d, x=% 4.0f, y=% 6.0f, z=% 4.0f,  flags=\"%s\", angle=%d, misc=%d },\n",
			ed_num, x, y, z, CalcFlagsString(flags), angle, tid);
}


void ProcessTHINGS(const char *level_name)
{
	byte * data;
	int length;

	data = the_wad->ReadLump("THINGS", &length);

	if (! data)
		FatalError("No THINGS lump in wad!");

	printf("%s_entities =\n", level_name);
	printf("{\n");

	int count = length / sizeof(raw_hexen_thing_t);

	raw_hexen_thing_t *raw = (raw_hexen_thing_t *) data;

	for (int i = 0 ; i < count ; i++, raw++)
	{
		VisitThing(raw);
	}

	printf("}\n\n");
}


const char *CalcLevelName(const char *filename)
{
	while (strstr(filename, ":"))
	{
		filename = strstr(filename, ":") + 1;
	}

	while (strstr(filename, "/"))
	{
		filename = strstr(filename, "/") + 1;
	}

	while (strstr(filename, "\\"))
	{
		filename = strstr(filename, "\\") + 1;
	}

	const char *end_p = strrchr(filename, '.');

	if (! end_p)
		end_p = filename + strlen(filename);

	// FIXME : do not overflow this buffer
	static char buffer[4096];

	const char *s = filename;
	char       *d = buffer;

	while (*s && s < end_p)
		*d++ = tolower(*s++);

	*d = 0;

	return buffer;
}


int main(int argc, char **argv)
{
	if (argc < 2)
	{
		FatalError("Missing wad filename.\n");
		return 0;
	}

	const char *wad_filename = argv[1];

	the_wad = wad_c::Open(wad_filename);

	if (! the_wad)
	{
		FatalError("No such file: %s\n", wad_filename);
		return 0;
	}

	const char *level_name = CalcLevelName(wad_filename);

	ProcessTHINGS(level_name);

	// close it
	delete the_wad;
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
