#!/bin/bash
set -e

rm -f TornadoTony.love
rm -f TornadoTony.zip

zip TornadoTony.zip README.txt GPL3_LICENSE.txt
zip TornadoTony.zip *.lua
zip TornadoTony.zip levels/*.lua
zip TornadoTony.zip gfx/*.*
zip TornadoTony.zip sfx/*.*
zip TornadoTony.zip music/*.*

mv TornadoTony.zip TornadoTony.love
