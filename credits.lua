--
--  List of Credits
--

Credits =
{
  { what = "", name = "", lic  = "CREDITS", is_title=true },
  { what = "", name = "", lic  = "" },
  { what = "", name = "", lic  = "" },

  { what = "Asset", name = "Author", lic  = "License", is_heading=true },
  { what = "", name = "", lic  = "" },

  {
    what = "Programming",
    name = "Andrew Apted",
    lic  = "GPL v3+"
  },

  { what = "", name = "", lic  = "" },

  --- Player ---

  {
    what = "Hero Sprites",
    name = "Tayoko",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Hero Armor",
    name = "LokiF",
    lic  = "CC-BY-SA 3.0"
  },

  {
    what = "Hero Pain Sound",
    name = "Michel Baradari",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Hero Jump Sound",
    name = "IgnasD",
    lic  = "CC0"
  },

  {
    what = "Hero Shoot Sound",
    name = "artisticdude",
    lic  = "CC0"
  },

  {
    what = "Hero Death Sound",
    name = "RottKing",
    lic  = "GPL v2"
  },

  { what = "", name = "", lic  = "" },

  --- Enemies ---

  {
    what = "Bat & Beetle",
    name = "Redshrike",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Scorpion Enemy",
    name = "Charles Gabriel",
    lic  = "CC-BY 3.0"
  },

  {
    what = "King Cobra",
    name = "Jordan Irwin",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Creeper Enemy",
    name = "StumpyStrust",
    lic  = "CC0"
  },

  {
    what = "Dragon Enemy",
    name = "Leonard Pabin",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Dragon Flames",
    name = "IsometricRobot.com",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Penguin Enemy",
    name = "Chrisblue",
    lic  = "CC-BY-SA 3.0"
  },

  {
    what = "Eye Ball",
    name = "Matthew Nash",
    lic  = "CC-BY-SA 3.0"
  },

  { what = "", name = "", lic  = "" },

  --- User Interface ---

  {
    what = "Sunset Background",
    name = "ansimuz",
    lic  = "CC0"
  },

  {
    what = "Desert Background",
    name = "PWL",
    lic  = "CC0"
  },

  {
    what = "Golden Trophy",
    name = "bevouliin.com",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Health Bar",
    name = "Jannax",
    lic  = "CC0"
  },

  {
    what = "Options Slider",
    name = "Rawdanitsu",
    lic  = "CC0"
  },

  { what = "", name = "", lic  = "" },

  --- Graphics ---

  {
    what = "Tree Sprites",
    name = "KnoblePersona",
    lic  = "CC-BY-SA 3.0"
  },

  {
    what = "Cherry & Dead Tree",
    name = "Glondo",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Palm Tree",
    name = "veeru5656",
    lic  = "CC0"
  },

  {
    what = "Bushy Tree",
    name = "airockstar",
    lic  = "CC0"
  },

  {
    what = "Cactus Sprite",
    name = "qubodup",
    lic  = "CC-BY-SA 3.0"
  },

  {
    what = "Icicle & Rock",
    name = "Tiny Speck",
    lic  = "CC0"
  },

  {
    what = "Barrel Sprite",
    name = "truezipp",
    lic  = "CC0"
  },

  {
    what = "Wooden Crate",
    name = "Cpt_Flash",
    lic  = "CC0"
  },

  {
    what = "Metal Box",
    name = "rubberduck",
    lic  = "CC0"
  },

  {
    what = "Spiked Obstacles",
    name = "dravenx",
    lic  = "GPL v3"
  },

  {
    what = "Greek Column",
    name = "Xavier4321",
    lic  = "CC0"
  },

  {
    what = "Snowman & Fence",
    name = "Aetherna",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Igloo",
    name = "Shozuki",
    lic  = "CC-BY 3.0"
  },

  { what = "", name = "", lic  = "" },

  {
    what = "Missile Sprites",
    name = "Rawdanitsu",
    lic  = "CC0"
  },

  {
    what = "Bomb Explosion",
    name = "J-Robot",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Explosion 2",
    name = "Amir027",
    lic  = "CC0"
  },

  {
    what = "Spinning Coin",
    name = "soundemperor",
    lic  = "CC-BY 4.0"
  },

  {
    what = "Red Gem",
    name = "soundemperor",
    lic  = "CC-BY 4.0"
  },

  {
    what = "Potion Sprite",
    name = "sunburn",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Apple & Banana",
    name = "mattn",
    lic  = "CC-BY-SA 3.0"
  },

  {
    what = "Jump Platform",
    name = "jcrown41 & andrewj",
    lic  = "CC0"
  },

  { what = "", name = "", lic  = "" },

  --- Sounds ---

  {
    what = "Victory Jingle",
    name = "Little Robot Sound Factory",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Game Over Jingle",
    name = "Little Robot Sound Factory",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Coin & Potion Sound",
    name = "wobbleboxx",
    lic  = "CC0"
  },

  {
    what = "Gem Sound",
    name = "p0ss",
    lic  = "CC-BY-SA 3.0"
  },

  {
    what = "Explosion Sound",
    name = "Michel Baradari",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Smash Sound",
    name = "Johnathan Roatch",
    lic  = "CC-BY-SA 3.0"
  },

  {
    what = "Boing Sound",
    name = "cfork & qubodup",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Clang Sound",
    name = "Bart",
    lic  = "CC0"
  },

  {
    what = "Spring Sound",
    name = "Blender Foundation",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Bat Death Sound",
    name = "Dan Knoflicek",
    lic  = "CC-BY 3.0"
  },

  {
    what = "Dragon Pain",
    name = "artisticdude",
    lic  = "CC0"
  },

  {
    what = "Dragon Flames",
    name = "AntumDeluge",
    lic  = "CC0"
  },

  {
    what = "Creeper Pain",
    name = "artisticdude",
    lic  = "CC0"
  },

  {
    what = "Creeper Laugh",
    name = "WeaponGuy",
    lic  = "CC0"
  },

  {
    what = "Creeper Shoot",
    name = "Little Robot Sound Factory",
    lic  = "CC-BY 3.0"
  },

  --- Music ---

  { what = "", name = "", lic  = "" },

  {
    what = "Title Music",
    name = "Emma MA",
    lic  = "CC0",
    file = "unsoliticed_trailer.ogg"
  },

  {
    what = "Level Music",
    name = "jukeri12",
    lic  = "CC-BY-SA 3.0",
    file = "riding_supernova.ogg"
  },

  {
    what = "Level Music",
    name = "nene",
    lic  = "CC0",
    file = "opening.ogg"
  },

  {
    what = "Level Music",
    name = "Pant0don",
    lic  = "CC-BY-SA 3.0",
    file = "mystical_world.ogg"
  },

  {
    what = "Level Music",
    name = "Patrick de Arteaga",
    lic  = "CC-BY 3.0",
    file = "mision_en_su_jardin.ogg"
  },

  {
    what = "Game Over Music",
    name = "The Cynic Project",
    lic  = "CC0",
    file = "hella_bumps.ogg"
  },
}

