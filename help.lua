--
--  Help text
--
--  Copyright (C) 2017  Andrew Apted
--
--  Under the GNU General Public License (GPL), version 3 or
--  (at your option) any later version.
--

Help_height = 1666

Help =
{
  { t="STORY", title=1 },

  "You are one of the Tornado Twelve, a secret military experiment to create super-soldiers.  You became stronger, able to run as fast as the wind, and drawing energy from the cosmic rays that constantly bombard the Earth.",

  "However there was a problem... a BIG problem...  Your body has no way to release the excess cosmic energy.  So you must run, run constantly, not even stopping for a few seconds, to prevent a build-up of energy from killing you.",

  "With nothing left to lose, you enter the toughest and deadliest competition on the planet : the HADES TRAIL.  Many men and women have perished attempting its cruel and savage challenges.  If YOU can win it, you can use the prize money to track down the scientist that did this to you, and perhaps end your nightmare once and for all.",

  "",
  { t="HOW TO PLAY", title=1 },

  "The goal of each level is to survive to the end, and with the highest score possible.  Your score can be increased by picking up certain items, like coins and gems, but it is also decreased when using bombs, so use them sparingly!",

  "If you die on a level, and your entry score was 3000 or higher, then you can sacrifice 3000 points to replay the level.  If your entry score was less than 3000, then it is game over I'm afraid!",

  "You enter each level with 100% health.  Your health is shown in the top right of the screen.  Running into obstacles, touching enemies or being hit by missiles will reduce your health, and is often fatal.  You can increase your health by eating fruit and drinking health potions.",

  "Some obstacles can be destroyed, either by bombs or by simply running into them.  There are also indestructible objects, such as the metal crates, which cannot be destroyed even with hundred bombs!",

  "Bonus levels are special levels where the goal is to simply collected as much loot as possible.  They can only be played once, and if you die then you do not lose 3000 points, but you forfeit any of the loot you collected.",

  "",
  { t="CONTROLS", title=1 },

  { t="LEFT ARROW : move left", center=2, gap=2 },
  { t="RIGHT ARROW : move right", center=2, gap=2 },
  { t="B or CTRL : shoot a bomb", center=2, gap=2 },
  { t="SPACE : jump", center=2 },
}

-- vi:wrap
