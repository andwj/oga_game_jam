
=================
 \             /
  TORNADO  TONY
 /             \
=================

by Andrew Apted


INTRODUCTION

This is an action game which I created for the OpenGameArt Game Jam,
which was held during the month of July, 2017.




COPYRIGHT and LICENSE
 
|  Tornado Tony : an action game
|
|  Copyright (C) 2017 Andrew Apted, et al
|
|  Amber Crux is free software; you can redistribute it and/or modify
|  it under the terms of the GNU General Public License as published
|  by the Free Software Foundation; either version 3 of the License,
|  or (at your option) any later version.
|
|  Amber Crux is distributed in the hope that it will be useful, but
|  WITHOUT ANY WARRANTY; without even the implied warranty of
|  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
|  GNU General Public License for more details.
|
|  You should have received a copy of the GNU General Public License
|  along with this software.  If not, please visit the following
|  web page: http://www.gnu.org/licenses/gpl.html
 
See the file 'GPL3_LICENSE.txt' in a source or binary package
for the complete text of the GNU GPLv3.  This license covers the
Lua scripts, which are the source code of the game.

The media files (i.e. the artwork) are covered by other licenses.
These licenses, such as CC-BY, CC-BY-SA, and CC0, all guarantee the
right to distribute the media (with or without modifications).
Please see credits.lua for the complete list.

