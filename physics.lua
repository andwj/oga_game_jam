--
--  Physics for players and other entities
--
--  Copyright (C) 2017  Andrew Apted
--
--  Under the GNU General Public License (GPL), version 3 or
--  (at your option) any later version.
--


CAMERA_Z    = 120
CAMERA_BACK = 55

PLAYER_LIMIT_X = 208


GRAVITY = 1000.0

FRICTION = 3.2

MOVE_ACCEL = 800
JUMP_ACCEL = 520

FORWARD_SPEED = 300
TURBO_SPEED   = 480



function entity_spawn(info, x, y, z)
  local ent =
  {
    info = info,

    x = x,
    y = y,
    z = z,

    mom_x = 0,
    mom_y = 0,
    mom_z = 0,

    health = info.health,  -- can be NIL

    spawn_time = Game.time,
  }

  if info.quads then
    ent.quad = info.quads[1][1]

    if info.anim_rate then
      -- make the coins (etc) be out-of-sync, but in a visually
      -- pleasing way when the spaced apart (by depth).
      ent.anim = ((y / 32) % 16) / 16
      ent.anim = ent.anim * info.tile_size[1]
    end
  end

  table.insert(ActiveEnts, ent)

  return ent
end



function entity_spawn_by_name(name, x, y, z)
  local info = ENTITY_TYPES[name]

  if not info then
    error("Unknown entity : " .. tostring(name))
  end

  return entity_spawn(info, x, y, z)
end



function physics_test_collision(e1, e2, delta_x, delta_y, delta_z, no_depth_check)
  --
  -- check if entity e1 is occupying the same area (on-screen) as e2.
  -- returns either true or nil.
  --
  -- delta_x/y/z are optional, if present they apply to the first entity.
  --

  -- cannot hit oneself
  if e1 == e2 then
    return nil
  end

  -- dead objects must be ignored
  if e1.dead or e2.dead then return nil end

  -- missiles can pass through their owner
  if e1.owner == e2 or e2.owner == e1 then
    return nil
  end

  if e1.no_collide or e1.info.no_collide then return nil end
  if e2.no_collide or e2.info.no_collide then return nil end

  -- check collision along depth plane
  if not no_depth_check and math.abs(e1.y - e2.y) > 8 then
    return nil
  end

  local ex, ey, e_scale, ew, eh = transform_entity(e1, delta_x, delta_y, delta_z)
  local fx, fy, f_scale, fw, fh = transform_entity(e2)

  -- first do a fairly quick bbox test
  if ex >= fx + fw then return nil end
  if fx >= ex + ew then return nil end

  if ey >= fy + fh then return nil end
  if fy >= ey + eh then return nil end

  -- now perform the more expensive trapezoid tests

  for _,AA in ipairs(e1.info.trapezoids) do
  for _,CC in ipairs(e2.info.trapezoids) do
    for ai = 1, #AA - 1 do
    for ci = 1, #CC - 1 do
      local TA = AA[ai]
      local TB = AA[ai + 1]

      local TC = CC[ci]
      local TD = CC[ci + 1]

      local ax1 = ex + TA.x1 * ew
      local ax2 = ex + TA.x2 * ew
      local ay  = ey + TA.y  * eh

      local bx1 = ex + TB.x1 * ew
      local bx2 = ex + TB.x2 * ew
      local by  = ey + TB.y  * eh

      local cx1 = fx + TC.x1 * fw
      local cx2 = fx + TC.x2 * fw
      local cy  = fy + TC.y  * fh

      local dx1 = fx + TD.x1 * fw
      local dx2 = fx + TD.x2 * fw
      local dy  = fy + TD.y  * fh

      if geom.trapezoids_intersect(ax1,ax2,ay, bx1,bx2,by, cx1,cx2,cy, dx1,dx2,dy) then
        return true
      end
    end -- ai, ci
    end
  end -- AA, CC
  end

  return nil
end



function entity_become_explosion(ent, only_partial)
  ent.dead  = true
  ent.dying = true

  ent.old_info = ent.info

  ent.info = ENTITY_TYPES.explosion

  if only_partial then
    ent.anim = 4.0
    ent.quad = ent.info.quads[5][1]
  else
    ent.anim = 0.0
    ent.quad = ent.info.quads[1][1]
  end

  ent.z = ent.z + 50

  ent.mom_z = 100

  -- move slightly toward camera, good for objects in a row
  ent.y = ent.y - 0.5
end



function entity_prune_dead_stuff()
  -- prune completely dead objects from the list (except players).
  -- we also handle static stuff which we have gone off-screen.

  -- must step backwards through the list
  for i = #ActiveEnts, 1, -1 do
    local ent = ActiveEnts[i]

    if ent.y < Game.cam_y then
      if ent.info.kind == "obstacle" or
         ent.info.kind == "item" or
         ent.info.kind == "enemy" or
         ent.info.kind == "missile"
      then
        ent.dead = true
      end
    end

    if ent.dead and not ent.dying and ent ~= Player then
      table.remove(ActiveEnts, i)
    end
  end
end


---=== PLAYER HANDLING ======-------------------------------------


function player_spawn()
  -- places the player into the level
  local p = Player

  p.info = ENTITY_TYPES.player

  p.dead    = false
  p.dying   = false
  p.no_draw = false

  p.x = 0
  p.y = 0
  p.z = 0

  p.mom_x = 0
  p.mom_z = 0

  p.fire_time = 0.1
  p.jump_time = 0.1

  -- initial sprite is standing frame
  p.quad = p.info.quads[1][3]

  p.score  = Game.enter_score
  p.health = p.info.health

  table.insert(ActiveEnts, p)
end



function missile_launch(kind, owner, delta_x, delta_y, delta_z,
                        mom_x, mom_y, mom_z)

  local m = entity_spawn_by_name(kind, owner.x + delta_x, owner.y + delta_y, owner.z + delta_z)

  m.owner = owner

  m.mom_x = mom_x
  m.mom_y = mom_y
  m.mom_z = mom_z

  if m.info.life_time then
    m.explode_time = Game.time + m.info.life_time
  end

  if m.info.sound then
    start_sound(m.info.sound)
  end

  return m
end



function player_hurt(damage)
  if damage >= Player.health then
    player_die()
    return
  end

  Player.health = Player.health - damage

  if damage >= 5 then
    start_sound(Sfx.pl_pain)
  end

  -- TODO  make another function for this
  local w = Player.info.tile_W * Player.info.scale
  local h = Player.info.tile_H * Player.info.scale

  local E = particle_effect("pl_pain", Player.x, Player.y - 1, Player.z + h / 2)

  E.mom_y = Game.forward_speed
end



function player_hit_by_missile(mis)
  mis.no_collide = true

  -- ignore missiles from dead monsters
  -- [ prevent mutual destruction scenarios ]
  if mis.owner and mis.owner.dead then
    return
  end

  player_hurt(mis.info.damage)

  if mis.info.name == "red_bomb" then
    entity_become_explosion(mis)

    start_sound(Sfx.explode)
  end
end



function player_grab_item(ent)
  local info = ent.info

  ent.dead = true

  if info.sound then
    start_sound(info.sound)
  end

  if info.add_score then
    Player.score = Player.score + info.add_score
  end

  if info.add_health then
    Player.health = Player.health + info.add_health
    Player.health = math.min(Player.health, Player.info.health)
  end
end



function player_find_collision_ent()
  -- this returns the first entity hit, or nil if none.
  -- results can include a pickup item or enemy missile.

  for _,ent in ipairs(ActiveEnts) do
    if physics_test_collision(Player, ent, delta_x, delta_y, delta_z) then
      return ent
    end
  end

  return nil
end



function player_can_be_nudged(delta_x, delta_y, delta_z)
  for _,ent in ipairs(ActiveEnts) do
    if ent.info.kind == "obstacle" then
      if physics_test_collision(Player, ent, delta_x, delta_y, delta_z) then
        return false
      end
    end
  end

  -- nothing solid was in the way
  return true
end



function player_move_out_of_the_way()
  --
  -- if the player would collide with something in the world,
  -- this sees if we can move a small distance out of the way.
  --
  -- returns dist number if we moved, nil otherwise.
  --

  -- try the UP direction first, important for jumping on things
  for dist = 1, 16 do
    local dz = dist

    if player_can_be_nudged(0, 0, dz) then
      Player.z = Player.z + dz
      Player.bumped_up = true

      -- do not hurt or play nudge sound
      return true
    end
  end


  for dist = 1, 9 do
  for side = 2, 4 do

    local dx = 0
    local dz = 0

    if side == 2 then dx =  dist end
    if side == 3 then dx = -dist end
    if side == 4 then dz = -dist end

    if dz < 0 and Player.z + dz < 0 then
      -- we cannot move the player below the ground
    else
      if player_can_be_nudged(dx, 0, dz) then
        Player.x = Player.x + dx
        Player.z = Player.z + dz

        if dist > 2 then
          start_sound(Sfx.pl_nudge)
        end

        return true
      end
    end

  end -- dist, side
  end

  -- nothing was possible
  return nil
end



function player_smash_obstacle(ent)
  -- check if enough health

  if not ent.health then
    -- indestructable!
    start_sound(Sfx.clang)
    player_die()
    return
  end

  -- Ok --

  if ent.info.sound then
    start_sound(ent.info.sound)
  else
    start_sound(Sfx.smash)
  end

  entity_become_explosion(ent, "only_partial")

  -- this may kill the player!
  player_hurt(ent.health)
end



function player_run_forward()
  if Player.dead then
    -- slow down when player dies
    Game.forward_speed = math.max(Game.forward_speed - 180 * Game.dt, 0)
  elseif Game.lev_info.is_bonus then
    Game.forward_speed = TURBO_SPEED
  else
    Game.forward_speed = FORWARD_SPEED
  end

  Player.y = Player.y + Game.forward_speed * Game.dt
end



function player_update_camera()
  local back_d = 0

  -- this makes it appear the player runs into the distance
  if Game.level_over then
    back_d = (Game.time - Game.level_over) - 4
    if back_d < 0 then
      back_d = 0
    else
      if back_d > 1 then back_d = back_d ^ 1.7 end

      back_d = back_d * 40

      -- make the health bar ramp up to 100% too
      if Player.health < 100 then
        Player.health = Player.health + 30 * Game.dt
        Player.health = math.min(Player.health, 100)
      end
    end
  end

  Game.cam_y = Player.y - (CAMERA_BACK + back_d)
  Game.cam_x = Player.x * 0.55

  -- dead player will get negative Z, so clamp it
  Game.cam_z = CAMERA_Z + math.max(Player.z, 0) * 0.5
end



function player_touch_enemy(mon)
  local damage = mon.info.damage or mon.info.health
  assert(damage)

  if mon.info.kind == "boss" then
    enemy_die(mon)
  else
    -- the monster will now be inert
    mon.no_collide = true
  end

  -- this may kill the player!
  player_hurt(damage)

  -- player jumps up a bit
  if not Player.dead then
    Player.mom_z = Player.mom_z + JUMP_ACCEL * 0.7
  end
end



function player_input()
  if Player.dead then return end

  local left  = love.keyboard.isDown("left")
  local right = love.keyboard.isDown("right")

  if left  then Player.mom_x = Player.mom_x - MOVE_ACCEL * Game.dt end
  if right then Player.mom_x = Player.mom_x + MOVE_ACCEL * Game.dt end

  -- aiming bombs is disabled
  Player.bomb_dx = 0
--  if left  then Player.bomb_dx = -1 end
--  if right then Player.bomb_dx =  1 end

  local jump = love.keyboard.isDown(" ")

  -- the "bumped_up" check allows spring-boarding off obstacles
  -- [ otherwise the player must be on the ground ]
  if jump and
     (Player.z == 0 or Player.bumped_up) and
     Game.time > Player.jump_time
  then
    Player.mom_z = JUMP_ACCEL
    Player.jump_time = Game.time + 0.05

    start_sound(Sfx.pl_jump1)
  end
end



function player_input_keypress(key)
  if Player.dead then return end

  local fire = (key == "b" or key == "lctrl" or key == "rctrl")

  if fire and Game.time > Player.fire_time and not Game.level_over then
    -- prevent firing again immediately
    Player.fire_time = Game.time + 0.05

    missile_launch("bomb", Player, 0, 5, 30,
                   Player.x * -0.05, Game.forward_speed*1.7, JUMP_ACCEL*0.41)

    Player.score = math.max(0, Player.score - BOMB_COST)
  end
end



function player_die()
  -- Note : health will reduce to zero (see player_death_think)

  Player.dead  = true
  Player.dying = true
  Player.death_time = Game.time

  -- setup momentum
  local dx = -1
  if Player.mom_x < 0 then dx = 1 end

  Player.mom_x = dx * 100
  Player.mom_z = JUMP_ACCEL * 0.4

  start_sound(Sfx.pl_death)
end



function player_physics()
  local p = Player

  if p.dead then return end

  -- prevent player from going off the screen (horizontally)
  if p.x > PLAYER_LIMIT_X then
    p.x = PLAYER_LIMIT_X

  elseif p.x > PLAYER_LIMIT_X * 0.8 then
    local d = math.abs(PLAYER_LIMIT_X - p.x) * 5
    p.mom_x = math.min(p.mom_x, d)

  elseif p.x < -PLAYER_LIMIT_X then
    p.x = -PLAYER_LIMIT_X

  elseif p.x < -PLAYER_LIMIT_X * 0.8 then
    local d = math.abs(-PLAYER_LIMIT_X - p.x) * 5
    p.mom_x = math.max(p.mom_x, -d)
  end

  -- player collision detection --

  -- this will be set if the player was nudged upward
  p.bumped_up = false

  local hit

  -- need a loop to handle cases where we hit something non-solid
  -- (like a pickup item) but also hit something solid.

  for loop = 1, 9 do
    if p.dead then return end

    hit = player_find_collision_ent()

    if not hit then break; end

    if hit.info.kind == "item" then
      player_grab_item(hit)
      -- continue loop (assume item was marked dead)

    elseif hit.info.kind == "missile" then
      player_hit_by_missile(hit)
      -- continue loop (assume missile was marked dead, or player died)

    elseif hit.info.kind == "enemy" or hit.info.kind == "boss" then
      player_touch_enemy(hit)
      -- continue loop (in case enemy was killed, though usually player is killed)

    elseif hit.info.kind == "jump_pad" then
      Player.mom_z = JUMP_ACCEL * 1.5

      -- prevent touching this object again
      hit.no_collide = true

      start_sound(Sfx.spring)

    elseif hit.info.insta_kill then
      start_sound(Sfx.clang)
      player_die()

    else
      -- player has hit an obstacle, see if we only brushed it
      -- [ NOTE : this only checks for obstacles ]
      local did_move = player_move_out_of_the_way()

      if did_move then
        -- do NOT continue loop, prevent being moved back by another obstacle
        break;
      end

      player_smash_obstacle(hit)
      -- continue loop (assume the obstacle was smashed and marked dead)
    end
  end
end



function player_death_think()
  -- use death sprite
  Player.quad = Player.info.quads[1][5]

  if Player.health > 0 then
    Player.health = Player.health - 150 * Game.dt
    Player.health = math.max(Player.health, 0)
  end

  if Player.dying then
    if Game.time > Player.death_time + 2 then
      -- fully dead now

      Player.dying = false
      Player.mom_x = 0

      return
    end
  end
end



function player_think()
  local p = Player

  if p.dead then
    player_death_think()
    return
  end

  -- animate the character
  local walk_frame = math.floor(Game.time * 8) % 4

  p.quad = p.info.quads[2 + walk_frame][3]
end



---=== ENEMY HANDLING ======-------------------------------------


function enemy_die(ent)
  local info = ent.info

  ent.dead  = true
  ent.dying = true
  ent.death_time = Game.time

  if info.sounds.death then
    start_sound(info.sounds.death)
  elseif info.sounds.pain then
    start_sound(info.sounds.pain)
  end

  entity_become_explosion(ent)

  if info.movement == "creeper" then
    ent.mom_x = ent.mom_x / 2
    ent.mom_y = Game.forward_speed - 60
    ent.mom_z = -30
  end

  for _,body in ipairs(ActiveEnts) do
    if body.info.kind == "body" and body.head == ent then
      body.explode_time = Game.time + 0.2 + math.random() * 1.5

      body.mom_y = ent.mom_y
      body.mom_x = body.mom_x / 2 + (math.random() - 0.5) * 20
      body.mom_z = -10 + (math.random() - 0.8) * 10
    end
  end
end



function enemy_hurt(ent, damage)
  assert(damage)

  if damage >= ent.health then
    enemy_die(ent)
    return
  end

  local info = ent.info

  ent.health = ent.health - damage

  if info.sounds.pain then
    start_sound(info.sounds.pain)
  end

  local E = particle_effect("pl_pain", ent.x, ent.y - 1, ent.z)

  E.mom_y = Game.forward_speed
end



function creeper_launch_lasers(ent)
  if Player.dead then
    if not ent.laughed and Game.time > Player.death_time + 2 then
      start_sound(ent.info.sounds.laugh)
      ent.laughed = true
    end

    return
  end

  local mom_y = -20
  local mom_x = (Player.x + Player.mom_x * 0.5 - ent.x) * 1.8
  local mom_z = (10 - ent.z) * 2.0

  local L = missile_launch("laser_beam", ent, 0, -10, -70,
                           mom_x, mom_y, mom_z)

  -- use the reddish one
  L.quad = L.info.quads[3][1]

  start_sound(ent.info.sounds.shoot)
end



function creeper_fly(ent)
  if not ent.fire_time then
    ent.fire_time = Game.time
  end

  if not ent.attack_time then
    ent.attack_time = Game.time + 2 + math.random() * 2
  end

  -- firing --

  local t = Game.time - ent.attack_time

  local where = ent.misc or 0

  if t >= 1.0 and Game.time > ent.fire_time then
    creeper_launch_lasers(ent)

    ent.fire_time = Game.time + 0.28

    -- slow fire when there are two creepers
    if where ~= 0 then
      ent.fire_time = ent.fire_time + 0.10
    end

    if t >= 2.0 then
      ent.attack_time = Game.time + 1 + math.random() * 2
    end
  end

  -- movement --

  ent.y = ent.y + Game.forward_speed * Game.dt

  local targ_dist = 40
  local targ_z    = 60

  if t >= 0 and t <= 2.2 then
    targ_dist = 180
    targ_z    = 240
  end

  targ_z = targ_z + (math.sin(Game.time * 2.88) + 1) * 100

  local cur_dist = ent.y - Player.y

  local dy = signed_clamp(targ_dist - cur_dist, 100)
  local dz = signed_clamp(targ_z - ent.z, 30)

  ent.y = ent.y + dy * 5.0 * Game.dt

  if where < 0 then
    ent.x = math.sin(Game.time * 1.50) * 100 + 115
  elseif where > 0 then
    ent.x = math.sin(Game.time * 1.55) * 100 - 115
  else
    ent.x = math.sin(Game.time * 1.50) * 200
  end

  ent.mom_z = dz * 6
end



function creeper_body_think(ent)
  if ent.head.dead then
    -- waiting to explode....
    return
  end

  local prev = ent.prev_piece

  ent.y = prev.y + 20 + 10 * math.sin(Game.time * 0.3)

  local dx = prev.x - ent.x
  local dz = prev.z - ent.z

  ent.mom_x = dx * 35
  ent.mom_z = dz * 20
end



function boss_left_right_distances(ent)
  local left  = 300
  local right = 300

  for _,oth in ipairs(ActiveEnts) do
    if oth.info.kind == "boss" and oth ~= ent then
      local dx = oth.x - ent.x
      local dy = oth.y - ent.y
      local dz = oth.z - ent.z

      if math.abs(dy) < 50 then
        if dx > 0 then right = math.min(right, dx) end
        if dx < 0 then left  = math.min(left, -dx) end
      end
    end
  end

  return left, right
end



function bat_fly(ent)
  if ent.y < Game.cam_y then
    ent.y = ent.y + 1000
    ent.action = nil
    ent.dropped_bomb = nil
    return
  end

  if not ent.action then
    local r = math.random()

        if r < 0.15 then ent.action = "drop"
    elseif r < 0.30 then ent.action = "swoop"
    else                 ent.action = "hover"
    end
  end

  local dist = math.abs(ent.y - Player.y)

  if ent.action == "drop" then
    ent.z = 20 + clamp(0, 300 - math.abs(dist - 500), 300)

    if ent.z >= 295 and not ent.dropped_bomb and ent.misc == 1 then
      local mom_x = (Player.x - ent.x) / 2

      local bomb = missile_launch("red_bomb", ent, 0, 5, -30,
                                  mom_x, 40, -20)
      ent.dropped_bomb = true
    end
  end

  if ent.action == "swoop" then
    ent.z = 20 + clamp(0, dist * 0.25, 200)
  end

  if ent.action == "hover" then
    ent.z = 40 + math.sin(Game.time * 1.9) * 170
  end
end



function dragon_fly(ent)
  if not ent.p_dist then
    ent.p_dist = ent.y - Player.y
  end

  if not ent.action then
    local r = math.random()

        if r < 0.10 then ent.action = "kamikaze"
    elseif r < 0.75 then ent.action = "attack"
    else                 ent.action = "fly_over"
    end
  end

  local left, right = boss_left_right_distances(ent)

  ent.mom_x = 0

  if left == right then
    -- track towards player
    if ent.x < Player.x then
      ent.mom_x =  140
    else
      ent.mom_x = -140
    end

  else
    -- avoid other dragons
    if left < right then
      ent.mom_x = 80
    else
      ent.mom_x = -80
    end
  end

  if ent.action == "circle_back" then
    local t = Game.time - ent.circle_time

    if not ent.circle_side then
      if right > left then ent.circle_side = 1
      elseif right < left then ent.circle_side = -1
      elseif ent.x < 0 then ent.circle_side = 1
      else ent.circle_side = -1
      end
    end

    ent.p_dist = ent.p_dist + 130 * Game.dt

    ent.mom_x = 0

    if t > 6 then
      ent.action = nil
    else
      if ent.circle_side < 0 then
        ent.mom_x = (3 - t) * 60
      else
        ent.mom_x = (t - 3) * 60
      end
    end

  elseif ent.action == "up_back" then
    local t = Game.time - ent.circle_time

    ent.p_dist = ent.p_dist + 160 * Game.dt

    ent.mom_z = 0

    if t > 6 then
      ent.action = nil
    else
      ent.mom_z = (3 - t) * 80
    end

  elseif ent.action == "fly_over" or ent.action == "kamikaze" then
    ent.p_dist = ent.p_dist - 500 * Game.dt

    if ent.action == "kamikaze" then
      ent.z = 24 + (clamp(0, ent.p_dist, 500) / 10) ^ 1.5
    else
      ent.mom_z = math.sin(Game.time * 1.3) * 100
    end

  elseif ent.p_dist > 70 and ent.p_dist < 90 then
    -- breathe some fire!!

    if ent.z > 70 then ent.mom_z = -130 end
    if ent.z < 50 then ent.mom_z =  50  end

    if ent.z < 74 and ent.fire_mode == nil then
      ent.fire_mode = "on"
      ent.fire_time = Game.time

      -- these are updated every frame (see end of this function)
      ent.flame1 = missile_launch("dragon_flame", ent, 0, 0, 0, 0, 0, 0)

      start_sound(Sfx.flames)
    end

    if ent.fire_mode then
      local t = Game.time - ent.fire_time

      if t >= 4 then
        ent.flame1.dead = true
        ent.flame1      = nil

        ent.fire_mode = nil
        ent.action = "circle_back"
        ent.circle_time = Game.time
        if rand_odds(30) then ent.action = "up_back" end
      end
    end

  else  -- attack approach

    ent.p_dist = ent.p_dist - 350 * Game.dt

    ent.mom_z = math.sin(Game.time * 1.7) * 100

    if ent.p_dist < 300 then
      if ent.z > 60 then ent.mom_z = -50 else ent.mom_z = 50 end
    end
  end

  ent.y = Player.y + ent.p_dist

  if ent.flame1 then
    ent.flame1.y = ent.y - 5
    ent.flame1.x = ent.x
    ent.flame1.z = ent.z - 80

    -- see if player is hurt by the flame
    local is_hurt = physics_test_collision(Player, ent.flame1, 0, 0, 0, "no_depth_check")

    if is_hurt and (not ent.hurt_time or Game.time >= ent.hurt_time) then
      player_hurt(5)
      ent.hurt_time = Game.time + 0.1
    end
  end

  -- fly past handling
  if ent.p_dist < -80 then
    ent.action = nil
    ent.p_dist = 1000
    ent.z = 200
    ent.mom_z = 0
  end
end



function boss_think(ent)
  if ent.info.movement == "creeper" then
    creeper_fly(ent)
    return
  end

  if ent.info.movement == "bat" then
    bat_fly(ent)
    return
  end

  if ent.info.movement == "dragon" then
    dragon_fly(ent)
    return
  end
end



function bomb_find_collision_ent(mis)
  for _,ent in ipairs(ActiveEnts) do
    if ent.info.kind == "obstacle" or
       ent.info.kind == "enemy"    or
       ent.info.kind == "boss"     or
       ent.info.kind == "body"
    then
      if physics_test_collision(mis, ent) then
        return ent
      end
    end
  end

  return nil  -- none found
end



function bomb_check_collisions(mis)
  -- off-screen?
  if mis.y < Game.cam_y + 2 then return end

  if mis.x < -PLAYER_LIMIT_X * 8 then return end
  if mis.x >  PLAYER_LIMIT_X * 8 then return end

  -- going backwards?
  if mis.mom_y < 0 then return end

  -- bombs are inert once the player has died
  -- [ prevents mutual destruction scenarios ]
  if mis.owner and mis.owner.dead then return end

  -- look for a hit obstacle
  local hit = bomb_find_collision_ent(mis)

  if not hit then return end

  if hit.info.kind == "body" then
    hit = hit.head

    if hit.dead then return end
  end

  -- indestructable?  if so, bounce back (toward camera)
  if hit.health == nil then
    mis.mom_y = -120
    mis.explode_time = Game.time + 2

    start_sound(Sfx.boing)
    return
  end

  if hit.info.kind == "obstacle" then
    -- we blew it up
    hit.dead = true
  else
    enemy_hurt(hit, mis.info.damage)
  end

  -- turn bomb into explosion
  entity_become_explosion(mis)

  start_sound(Sfx.explode)
end



function entity_physics(ent)
  -- death physics are different
  -- [ dead player will fall off the screen ]
  if ent.dead and ent.info.kind == "player" then
    ent.x = ent.x + ent.mom_x * Game.dt
    ent.y = ent.y + ent.mom_y * Game.dt
    ent.z = ent.z + ent.mom_z * Game.dt

    if ent.info.death_gravity then
      ent.mom_z = ent.mom_z - GRAVITY * Game.dt * 0.5
    end

    return
  end

  -- apply XY momentum

  local on_ground = (ent.z == 0)

  if ent.mom_x ~= 0 then
    ent.x = ent.x + ent.mom_x * Game.dt
  end

  if ent.mom_y ~= 0 then
    ent.y = ent.y + ent.mom_y * Game.dt
  end

  if not ent.info.no_friction then
    ent.mom_x = ent.mom_x - ent.mom_x * FRICTION * Game.dt
    ent.mom_y = ent.mom_y - ent.mom_y * FRICTION * Game.dt
  end

  -- apply Z momentum and gravity

  if ent.mom_z ~= 0 then
    ent.z = ent.z + ent.mom_z * Game.dt

    if ent.z < 0 then
      if not on_ground and ent.info.bounces then
        ent.mom_z = JUMP_ACCEL * 0.5
      end

      ent.z = 0
      on_ground = true
    end
  end

  if ent.info.falls and not on_ground then
    local grav_dz = GRAVITY * ent.info.mass / 100

    ent.mom_z = ent.mom_z - grav_dz * Game.dt
  end
end



function entity_animate(ent)
  local info = ent.info

  -- handle animated objects
  -- the "anim" field is a fractional value: 0.0 <= anim < NUM_FRAMES

  if info.anim_rate then
    ent.anim = ent.anim + info.anim_rate * Game.dt

    if ent.anim >= info.tile_size[1] then
      if info.one_shot then
        ent.dead  = true
        ent.dying = false
        return
      end

      ent.anim = 0
    end

    local idx = 1 + math.floor(ent.anim)

    ent.quad = info.quads[idx][1]
  end
end



function entity_think(ent)
  if ent.particles then
    update_particle_group(ent)
  end

  if ent == Player then return end

  entity_animate(ent)

  if ent.dead then return end

  if ent.explode_time and Game.time >= ent.explode_time then
    if ent.info.name == "bomb" then
      start_sound(Sfx.explode)
    end

    entity_become_explosion(ent)
    return
  end

  if ent.info.kind == "missile" and ent.owner == Player then
    bomb_check_collisions(ent)
  end

  if ent.info.kind == "boss" then
    boss_think(ent)
  end

  if ent.info.kind == "body" then
    creeper_body_think(ent)
  end
end



function game_think(dt)
  Game.dt = dt

  if Game.in_menu or not Game.active then
    Game.menu_time = Game.menu_time + dt
    return
  end

  Game.time = Game.time + dt

  if Game.finished then
    game_finished_think()
    return
  end

  -- active game --

  player_run_forward()

  -- handle input
  player_input()

  -- run physics on all entities
  for i = 1, #ActiveEnts do
    entity_physics(ActiveEnts[i])
  end

  player_physics()

  -- run thinkers on all entities
  for i = 1, #ActiveEnts do
    entity_think(ActiveEnts[i])
  end

  player_think()

  entity_prune_dead_stuff()

  game_active_think()
end


