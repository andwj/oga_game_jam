--
--  Main program
--
--  Copyright (C) 2017  Andrew Apted
--
--  Under the GNU General Public License (GPL), version 3 or
--  (at your option) any later version.
--


-- note: many things are hard-coded to this screen size
SCREEN_W = 800
SCREEN_H = 600


Fonts =
{
  -- title
  -- score
  -- large
  -- normal
  -- small
}


Options =
{
  -- this ranges from 0.0 to 1.0, sets the master volume
  sound_volume = 0.5,

  -- this is currently either 0.0 (Off) or 1.0 (ON)
  music_volume = 1.0,

  -- this is either true or false
  fullscreen = false,
}


--
--  various graphics
--
--  [ entities store the image in their INFO table ]
--
Gfx =
{
  -- bg_xxxx     : background images

  -- particle    : the image used for particles

  -- ui_xxx      : various UI elements
}


--
--  sound info
--
Sfx =
{
  -- pl_jump1
  -- pl_pain
  -- pl_nudge
  -- pl_death

  -- smash
  -- boing
  -- clang
  -- spring
  -- explode
  -- flames

  -- jingle_lose
  -- jingle_win
}


--
-- the game state
--
Game =
{
  -- this is true when a game has been started
  active = false,

  -- this is true when game is over (player lost his last life, or has won)
  finished = false,

  -- this tells us whether we are in the menus
  in_menu = false,

  -- the current screen, can be NIL, "options", "help", "credits"
  screen = nil,

  -- the currently playing music track
  music  = nil,

  -- this is set while we transition between music tracks
  old_music = nil,

  -- time counter, used while game is active (and not in menus)
  time = 0,

  -- time counter when game is not active or menu/screens are shown
  menu_time = 0,

  -- current level number, and its information table
  level = 0,
  lev_info = {},

  -- current camera position
  cam_x = 0,
  cam_y = 0,
  cam_z = 0,

  forward_speed = 0,

  -- the obstacle which is next to be added to ActiveEnts
  obstacle_next = 0,

  -- the score at the start of the level
  enter_score = 0,

  -- this is true when we are fighting the end-of-level boss
  boss_fight = false,

  -- misc UI stuff
  menu_pos   = 1,
  option_pos = 1,
  help_pos   = 1,
}


--
-- player state
--

Player =
{
  -- location
  x = 0,
  y = 0,
  z = 0,

  -- current momentum
  mom_x = 0,
  mom_y = 0,
  mom_z = 0,
}



--
-- all entities to be drawn and run physics
--
-- includes the player, active missiles, and all enemies.
-- map obstacles will come and go, as needed.
--
ActiveEnts = {}



--
-- game-wide constants
--
INITIAL_SCORE = 10000
NEW_LIFE_COST = 3000
BOMB_COST = 10


-- physics are run at 100 fps
FRAME_TIME = (1 / 100)

-- accumulator for very small dt values
TIME_ACCUM = 0


---=== ENTITY DEFINITIONS and TEXT SCREENS =====-------------------


require "entities"

require "credits"

require "help"


---=== LEVEL DEFINITIONS ======-----------------------------------

require "levels/jungle_1"
require "levels/jungle_2"
require "levels/jungle_3"

require "levels/desert_1"
require "levels/desert_2"
require "levels/desert_3"

require "levels/ice_1"
require "levels/ice_2"
require "levels/ice_3"

require "levels/bonus_1"
require "levels/bonus_2"

-- FIXME REMOVE
require "levels/test"


LEVEL_LIST =
{
  --- Jungle 1 ---
  {
    obstacles   = test_entities,
    background  = "bg_mountain",
    floor_color = "green",
    music       = "foobie",

    bosses =
    {
      { name="bat", x=   0, y=700, z=100 },
      { name="bat", x=  90, y=730, z=100 },
      { name="bat", x= -90, y=730, z=100 },
      { name="bat", x= 180, y=760, z=100 },
      { name="bat", x=-180, y=760, z=100 },
    }
  },

  --- Jungle 2 ---
  {
    obstacles   = jungle_2_entities,
    background  = "bg_mountain",
    floor_color = "green",
    music       = "foobie",

    bosses =
    {
      { name="dragon", x=0, y=1000, z=250, },
    }
  },

  --- Jungle 3 ---
  {
    obstacles   = jungle_3_entities,
    background  = "bg_mountain",
    floor_color = "green",
    music       = "foobie",

    bosses =
    {
      { name="creeper", x=0, y=1000, z=100 },
    }
  },

  --- BONUS 1 ---
  {
    obstacles   = bonus_1_entities,
    background  = "bg_mountain",
    floor_color = "blue",
    music       = "foobie",
    is_bonus    = true,
  },

  --- Desert 1 ---
  {
    obstacles   = desert_1_entities,
    background  = "bg_desert",
    floor_color = "yellow",

    bosses =
    {
      { name="bat", x=   0, y=700, z=100 },
      { name="bat", x=  90, y=720, z=100 },
      { name="bat", x= -90, y=720, z=100 },
      { name="bat", x= 180, y=750, z=100 },
      { name="bat", x=-180, y=750, z=100 },

      { name="bat", x=   0, y=1100, z=120 },
      { name="bat", x=  90, y=1120, z=120 },
      { name="bat", x= -90, y=1120, z=120 },
      { name="bat", x= 180, y=1150, z=120 },
      { name="bat", x=-180, y=1150, z=120 },
    }
  },

  --- Desert 2 ---
  {
    obstacles   = desert_2_entities,
    background  = "bg_desert",
    floor_color = "yellow",
    bosses =
    {
      { name="creeper", x=0, y=1000, z=100 },
    }
  },

  --- Desert 3 ---
  {
    obstacles   = desert_3_entities,
    background  = "bg_desert",
    floor_color = "yellow",
    bosses =
    {
      { name="dragon", x= 200, y=1000, z=150, },
      { name="dragon", x=   0, y=1000, z=350, },
      { name="dragon", x=-200, y=1000, z=250, },
    }
  },

  --- BONUS 2 ---
  {
    obstacles   = bonus_2_entities,
    background  = "bg_desert",
    floor_color = "red",
    music       = "foobie",
    is_bonus    = true,
  },

  --- Ice 1 ---
  {
    obstacles   = ice_1_entities,
    background  = "bg_icy",
    floor_color = "icy",

    bosses =
    {
      { name="penguin", x=   0, y=700, z=100 },
      { name="penguin", x=  90, y=720, z=100 },
      { name="penguin", x= -90, y=720, z=100 },
      { name="penguin", x= 180, y=750, z=100 },
      { name="penguin", x=-180, y=750, z=100 },

      { name="penguin", x=   0, y=1100, z=120 },
      { name="penguin", x=  90, y=1120, z=120 },
      { name="penguin", x= -90, y=1120, z=120 },
      { name="penguin", x= 180, y=1150, z=120 },
      { name="penguin", x=-180, y=1150, z=120 },
    }
  },

  --- Ice 2 ---
  {
    obstacles   = ice_2_entities,
    background  = "bg_icy",
    floor_color = "icy",

    bosses =
    {
      { name="dragon", x= 200, y=1000, z=250, },
      { name="dragon", x=-200, y=1000, z=250, },
    }
  },

  --- Ice 3 ---
  {
    obstacles   = ice_3_entities,
    background  = "bg_icy",
    floor_color = "icy",

    bosses =
    {
      { name="creeper", x= 200, y=1000, z=100, misc= 1 },
      { name="creeper", x=-200, y=1000, z=100, misc=-1 },
    }
  },
}


---=== UTILITY FUNCTIONS ======-------------------------------------


require "geom"


function clamp(low, val, hi)
  if val < low then return low end
  if val > hi  then return hi  end

  return val
end

function signed_clamp(val, limit)
  if val >  limit then return  limit end
  if val < -limit then return -limit end

  return val
end


function rand_odds(chance)
  return (math.random() * 100) <= chance
end

function rand_range(L, H)
  return L + math.random() * (H - L)
end

function rand_int_range(L, H)
  if H < L then L,H = H,L end

  return math.floor(L + math.random() * (H - L + 0.999))
end



---=== USER SETTINGS ======---------------------------------------


SETTINGS_FILE = "settings.raw"


function save_user_settings()
  local vol = math.floor(Options.sound_volume * 100)
  local mus = math.floor(Options.music_volume * 100)

  local fsc = (Options.fullscreen and 1) or 0

  local s = string.format(" %d  %d  %d \n", vol, mus, fsc)

  love.filesystem.write(SETTINGS_FILE, s)
end



function load_user_settings()
  local s = love.filesystem.read(SETTINGS_FILE)

  if not s then
    -- file did not exist, that is OK
    return
  end

  local vol, mus, fsc = string.match(s, "([+-]?%d+)%s+([+-]?%d+)%s+([+-]?%d+)")

  if vol then
    Options.sound_volume = (0 + vol) / 100
  end

  if mus then
    Options.music_volume = (0 + mus) / 100
  end

  if fsc then
    Options.fullscreen = ((0 + fsc) > 0)
  end
end


---=== SOUND and MUSIC ======-------------------------------------


MUSIC_FADE_TIME = 2.2


MUSIC_LIST =
{
  title =
  {
    filename = "unsolicited_trailer.ogg",
    volume   = 0.8,
  },

  victory =
  {
    filename = "hella_bumps.ogg",
    volume   = 0.9,
  },

  foobie =
  {
    filename = "riding_supernova.ogg",
    volume   = 0.8,
  },
}


function load_a_sound(filename, max_poly)
  -- returns a SOUND_INFO table:
  --    max_poly   :  as given
  --    sources[]  :  all the sources (1 .. max_poly)

  if max_poly == nil then
     max_poly = 1
  end

  local SOUND_INFO =
  {
    max_poly = max_poly,
    sources  = {},
  }

  local sfx = love.audio.newSource("sfx/" .. filename, "static")
  assert(sfx)

  SOUND_INFO.sources[1] = sfx

  for i = 2, max_poly do
    SOUND_INFO.sources[i] = sfx:clone()
  end

  return SOUND_INFO
end



function start_sound(info)
  assert(info)

  local sfx = info.sources[1]
  assert(sfx)

  -- move to last in list
  if info.max_poly > 1 then
    table.remove(info.sources, 1)
    table.insert(info.sources, sfx)
  end

  -- if source is already playing, rewind it
  if sfx:isPlaying() then
    sfx:rewind()
  else
    sfx:play()
  end
end



function start_music(name)
  local info = MUSIC_LIST[name]

  if not info then
    error("Unknown music: " .. tostring(name))
  end

  -- already playing?
  if info == Game.music then return end

  local source = love.audio.newSource("music/" .. info.filename, "stream")
  assert(source)

  info.source = source

  -- fade in the new music over the old one
  if Game.music then
     Game.old_music = Game.music
     source:setVolume(0)
  else
     source:setVolume(info.volume * Options.music_volume)
  end

  source:setLooping(true)

  source:play()

  Game.music = info
  Game.music_time = Game.time
end



function music_think()
  -- this is for transitioning an old song to a new one

  if Game.old_music then
    local old_m = Game.old_music
    local new_m = Game.music

    local t = (Game.time - Game.music_time) / MUSIC_FADE_TIME

    if t >= 1 then
      old_m.source:setVolume(0)
      old_m.source:stop()

      new_m.source:setVolume(new_m.volume * Options.music_volume)

      -- the old source should be garbage collected
      Game.old_music.source = nil
      Game.old_music = nil

      return
    end

    local old_vol = old_m.volume * (1 - t)
    local new_vol = new_m.volume * t

    old_m.source:setVolume(old_vol * Options.music_volume)
    new_m.source:setVolume(new_vol * Options.music_volume)
  end
end



function update_sound_volume()
  love.audio.setVolume(Options.sound_volume)
end



function update_music_volume()
  -- when transitioning, the music_think() code will handle it
  if Game.music and not Game.old_music then
    local m = Game.music

    m.source:setVolume(m.volume * Options.music_volume)
  end
end


---=== MAJOR GAME FUNCTIONS ======----------------------------


function game_quit()
  save_user_settings()

  love.event.push("quit")

  Game.active  = false
  Game.in_menu = false
  Game.screen  = nil
end



function game_over(won)
  Game.finished = true
  Game.won      = won

  if not won then
    start_sound(Sfx.jingle_lose)
  end
end



function level_setup_obstacle(ent, ob)
  ent.start_x = ob.x
  ent.start_y = ob.y
  ent.start_z = ob.z

  -- these are mainly for enemies and special objects
  ent.angle   = ob.angle or 0
  ent.misc    = ob.misc  or 0
end



function level_add_new_obstacles()
  local lev_info = LEVEL_LIST[Game.level]

  local LOOK_AHEAD = 1000

  while 1 do
    if Game.obstacle_next > #lev_info.obstacles then
      break;
    end

    local ob = lev_info.obstacles[Game.obstacle_next]

    -- is the obstacle too far away to be rendered?
    if Player.y + LOOK_AHEAD < ob.y then
      break;
    end

    local info = ENTITY_MAP[ob.ed_num]
    if not info then
      error(string.format("Unknown entity type: %d", ob.ed_num))
    end

    local ent = entity_spawn(info, ob.x, ob.y, ob.z)

    level_setup_obstacle(ent, ob)

    Game.obstacle_next = Game.obstacle_next + 1
  end
end



function level_test_stuff()
  -- this is for adding objects for testing

  entity_spawn_by_name("beetle",  -60, 500, 0)
  entity_spawn_by_name("wooden_fence",  -140, 500, 0)
  entity_spawn_by_name("wooden_fence",  -100, 500, 0)
end



function level_init()
  local lev_info = LEVEL_LIST[Game.level]
  assert(lev_info)

  Game.lev_info = lev_info

  Game.active = true
  Game.finished = false
  Game.level_over = nil

  Game.in_menu = false
  Game.screen = nil

  Game.time   = 0
  Game.forward_speed = 0

  ActiveEnts = {}

  player_spawn()
  player_update_camera()

  Game.obstacle_next = 1

  -- ensure obstacles are sorted by Y coord
  table.sort(lev_info.obstacles,
      function(A, B) return A.y < B.y end)

  -- after player goes past last obstacle, the bosses cometh
  Game.boss_fight = false

  if # lev_info.obstacles == 0 then
    Game.boss_fight_y = 400
  else
    Game.boss_fight_y = lev_info.obstacles[# lev_info.obstacles].y + 200
  end

  level_add_new_obstacles()
  level_test_stuff()

  if lev_info.music then
    start_music(lev_info.music)
  end
end



function level_cleanup()
  ActiveEnts = {}

  collectgarbage("collect")
end



function game_start_new()
  Game.level = 1
  Game.enter_score = INITIAL_SCORE
  Game.won = false

  level_cleanup()
  level_init()
end



function game_restart_level()
  Game.enter_score = Game.enter_score - NEW_LIFE_COST

  level_cleanup()
  level_init()
end



function game_next_level()
  Game.enter_score = Player.score

  Game.level = Game.level + 1

  level_cleanup()
  level_init()
end



function spawn_creeper_body(head)
  local prev_piece = head

  for i = 1, 30 do
    local body = entity_spawn_by_name("creeper_body", head.x, head.y + i, head.z)

    body.head       = head
    body.prev_piece = prev_piece

    prev_piece = body
  end
end



function game_add_a_boss(B)
  local ent = entity_spawn_by_name(B.name, B.x, Player.y + B.y, B.z)

  Game.boss_total_health = Game.boss_total_health + ent.info.health

  if ent.info.movement == "creeper" then
    spawn_creeper_body(ent)
  end

  ent.misc = B.misc or 0
end



function game_spawn_bosses()
  Game.boss_total_health = 0

  if not Game.lev_info.bosses then return end

  for _,B in ipairs(Game.lev_info.bosses) do
    game_add_a_boss(B)
  end
end



function game_check_conditions()
  -- player died for last time?
  if Player.dead and not Player.dying then
    if Game.enter_score < NEW_LIFE_COST and not Game.lev_info.is_bonus then
      game_over(false)
    end
  end

  -- reached the boss fight?
  if not Game.boss_fight and not Game.level_over and
     not Player.dead and Player.y >= Game.boss_fight_y
  then
    Game.boss_fight = true
    game_spawn_bosses()
    return
  end

  -- boss fight has been won?
  if Game.boss_fight and not Player.dead then
    -- count number of active bosses
    local count = 0

    for _,ent in ipairs(ActiveEnts) do
      if ent.info.kind == "boss" then
        count = count + 1
      end
    end

    if count == 0 then
      Game.boss_fight = false
      Game.level_over = Game.time

      start_sound(Sfx.jingle_win)

      if Game.level == #LEVEL_LIST then
        start_music("victory")
      end
    end
  end

  -- time to enter the next level?
  if Game.level_over and Game.time > Game.level_over + 8 then

    -- when there are no more levels, game is finished
    if Game.level < #LEVEL_LIST then
      game_next_level()
    else
      game_over(true)

      -- player ran into distance, so make them disappear
      Player.no_draw = true
    end
  end
end



function game_active_think()
  player_update_camera()

  game_check_conditions()

  level_add_new_obstacles()
end



function game_finished_think()
  -- nothing to do
end



---=== RENDERING and PHYSICS ======---------------------------


require "render"

require "physics"



---=== RESOURCE LOADING ======------------------------------


function create_quad_array(info, W, H)
  local img = info.image
  local img_W, img_H = img:getDimensions()

  info.tile_W = math.floor(img_W / W)
  info.tile_H = math.floor(img_H / H)

  info.quads = {}

  for tx = 1, W do
    info.quads[tx] = {}

    for ty = 1, H do
      info.quads[tx][ty] = love.graphics.newQuad(
          (tx - 1) * info.tile_W,
          (ty - 1) * info.tile_H,
          info.tile_W, info.tile_H,
          img_W, img_H)
    end
  end
end



function load_all_fonts()
  Fonts.title  = love.graphics.setNewFont(36)
  Fonts.score  = love.graphics.setNewFont(30)
  Fonts.large  = love.graphics.setNewFont(24)
  Fonts.normal = love.graphics.setNewFont(20)
  Fonts.small  = love.graphics.setNewFont(16)
end



function load_all_entity_stuff()
  for _,info in pairs(ENTITY_TYPES) do
    if info.image_file then
      info.image = love.graphics.newImage("gfx/" .. info.image_file)
    end

    if info.tile_size then
      create_quad_array(info, info.tile_size[1], info.tile_size[2])
    end

    if info.sound_file then
      info.sound = load_a_sound(info.sound_file, info.max_poly)
    end

    if not info.sounds then
      info.sounds = {}
    end

    local S = info.sounds

    if S. pain_file then S. pain = load_a_sound(S. pain_file, 2) end
    if S.shoot_file then S.shoot = load_a_sound(S.shoot_file, 2) end
    if S.death_file then S.death = load_a_sound(S.death_file, 2) end
    if S.laugh_file then S.laugh = load_a_sound(S.laugh_file, 2) end
  end
end



function load_all_graphics()
  Gfx.bg_mountain = love.graphics.newImage("gfx/bg_mountain.png" )
  Gfx.bg_desert   = love.graphics.newImage("gfx/bg_desert.png" )
  Gfx.bg_icy      = love.graphics.newImage("gfx/bg_icy.png" )

  Gfx.particle    = love.graphics.newImage("gfx/particles.png")

  Gfx.ui_title    = love.graphics.newImage("gfx/ui_title.png")
  Gfx.ui_trophy   = love.graphics.newImage("gfx/ui_trophy.png")

  Gfx.ui_slider   = love.graphics.newImage("gfx/ui_slider.png")
  Gfx.ui_knob     = love.graphics.newImage("gfx/ui_knob.png")

  Gfx.ui_bar_base = love.graphics.newImage("gfx/ui_bar_base.png")
  Gfx.ui_bar_red  = love.graphics.newImage("gfx/ui_bar_red.png")
  Gfx.ui_bar_blue = love.graphics.newImage("gfx/ui_bar_blue.png")
end



function load_all_sounds()
  Sfx.smash    = load_a_sound("smash.wav",  2)
  Sfx.boing    = load_a_sound("boing.wav",  2)
  Sfx.clang    = load_a_sound("clang.wav",  1)
  Sfx.spring   = load_a_sound("spring.wav", 1)
  Sfx.explode  = load_a_sound("explode.wav", 2)
  Sfx.flames   = load_a_sound("flames.wav", 2)

  Sfx.pl_jump1 = load_a_sound("pl_jump1.ogg", 1)
  Sfx.pl_nudge = load_a_sound("pl_nudge.wav", 2)
  Sfx.pl_pain  = load_a_sound("pl_pain.wav",  2)
  Sfx.pl_death = load_a_sound("pl_death.wav", 1)

  Sfx.jingle_win  = load_a_sound("jingle_win.wav",  1)
  Sfx.jingle_lose = load_a_sound("jingle_lose.wav", 1)
end



---=== CALLBACKS FROM LÖVE2D ======------------------------------


function love.keypressed(key)
  if key == "kpenter" then
    key = "enter"
  end

  if key == "`" or key == "tab" then
    key = "escape"
  end

  if not Game.active and not Game.in_menu then
    Game.in_menu = true
    Game.screen  = nil
    return
  end

  -- outside of a game, send keys to the menu system
  if Game.in_menu then
    menu_key_pressed(key)
    return
  end

  -- when game is over, require a certain key to bring up the menus
  if Game.finished then
    if key == "escape" or
       key == "enter" or key == "return"
    then
      Game.in_menu = true
    end

    return
  end

  -- ability to bring up the main menu
  if key == "escape" and not Player.dying then
    Game.in_menu = true

    -- prevent selecting the "Start Game" option
    if Game.menu_pos < 2 then
      Game.menu_pos = 2
    end

    return
  end

  -- can replay the current level after dying
  if key == "enter" or key == "return" then
    if Player.dead and not Player.dying then
      if Game.lev_info.is_bonus then
        -- lose the loot!
        Player.score = Game.enter_score
        game_next_level()
      else
        game_restart_level()
      end

      return
    end
  end

  -- during the game, send key presses to the player
  if not Player.dead then
    player_input_keypress(key)
  end
end



function love.draw()
  if Game.active then
    draw_background()
    draw_ground()
    draw_all_entities()
    draw_player_stats()
  else
    draw_title_pic()
  end

  if Game.in_menu then
    menu_draw()
  end
end



function love.update(dt)
  dt = math.min(dt, 2000)

  TIME_ACCUM = TIME_ACCUM + dt

  while TIME_ACCUM >= FRAME_TIME do
    game_think(FRAME_TIME)

    music_think()

    TIME_ACCUM = TIME_ACCUM - FRAME_TIME
  end
end



function love.load()
  love.graphics.setBackgroundColor(0,0,0)

  math.randomseed(os.time())

  load_user_settings()

  update_sound_volume()

  if Options.fullscreen then
    update_fullscreen()
  end

  load_all_fonts()
  load_all_graphics()
  load_all_sounds()
  load_all_entity_stuff()

  build_entity_map()

  start_music("title")
end

