--
--  Startup Configuration
--
--  Copyright (C) 2017  Andrew Apted
--
--  Under the GNU General Public License (GPL), version 3 or
--  (at your option) any later version.
--


function love.conf(t)

  t.version = "0.9.1"

  t.identity = "Tornado_Tony"

  t.window.width  = 800
  t.window.height = 600

  t.window.title  = "Tornado Tony"

  -- disable some modules
  t.modules.joystick = false
  t.modules.physics  = false
end

