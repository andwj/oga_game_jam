--
--  Rendering and UI
--
--  Copyright (C) 2017  Andrew Apted
--
--  Under the GNU General Public License (GPL), version 3 or
--  (at your option) any later version.
--

EYE_DIST = 100

SQUARE_SZ = 64

GROUND_W  = 12
GROUND_H  = 13


FLOOR_COLORS =
{
  green  = {   0, 128,   0,    0,  84,   0 },
  blue   = {   0,   0, 192,    0,   0, 144 },
  red    = { 160,   0,   0,  112,   0,   0 },
  yellow = { 192, 176,   0,  160, 104,   0 },

  orange = { 192, 128,   0,  128,  64,   0 },
  cyan   = {   0, 128, 192,    0,  64, 144 },
  purple = { 176,  64, 176,  128,   0, 128 },
  icy    = { 192, 192, 192,   96,  96, 160 },
}


--
-- Fields of particle effect:
--
--  num1,   num2   : number of particles to spawn   [ 5, num1 ]
--
--  color1, color2 : range of colors (including alpha)  [ {255,255,255}, color1 ]
--  veloc1, veloc2 : range of velocities   [ {0,0}, -veloc1 ]
--  offset1, offset2 : range of offsets    [ {0,0}, -offset1 ]
--
--  size1,  size2  : range of sizes  [ 6, size1 ]
--  time1,  time2  : range of lifetimes  [ 4, time1 ]
--
--  alpha_decay    : subtract this from alpha every second     [ 0 ]
--  size_decay     : subtract this from size every second      [ 0 ]
--  decelerate     : subtract this from velocity every second  [ {0,0} ]
--
--  gravity        : how much affected by gravity, 1 = normal  [ 0.0 ]
--
PARTICLE_EFFECTS =
{
  defaults =
  {
    num1    = 5,
    color1  = { 255, 255, 255 },
    veloc1  = { 0, 0, 0 },
    offset1 = { 0, 0, 0 },
    size1   = 6,
    time1   = 3,
  },

  pl_pain =
  {
    num1    = 20,
    color1  = { 255, 0, 0 },

    size1   = 12,
    size_decay = 6,

    veloc1  = { -60, -120 },
    veloc2  = {  60,   60 },
    offset1 = { -15,  -19 },
    offset2 = {  15,   19 },

    gravity = 1,
  },
}



function transform_point(x, y, z)
  x = x - Game.cam_x
  y = y - Game.cam_y
  z = z - Game.cam_z

  if y < 1 then y = 1 end

  local x2 = 400 + 2 * x * EYE_DIST / y
  local y2 = 350 - z * EYE_DIST / y

  return x2, y2
end



function transform_size(size, y)
  y = y - Game.cam_y

  if y < 1 then y = 1 end

  return size * EYE_DIST / y
end


function transform_lighting(r, g, b, y)
  y = y - Game.cam_y

  if y < 200 then return r, g, b end

  y = y - 200
  y = y / 1400

  if y > 1 then y = 1 end

  y = (1 - y) ^ 0.9

  return r * y, g * y, b * y
end


function transform_entity(ent, delta_x, delta_y, delta_z)
  local info = ent.info

  if not delta_x then
    delta_x = 0
    delta_y = 0
    delta_z = 0
  end

  local x, y  = transform_point(ent.x + delta_x, ent.y + delta_y, ent.z + delta_z)
  local scale = transform_size(info.scale, ent.y)

  local w, h = 1, 1

  if info.tile_W then
    w = info.tile_W
    h = info.tile_H
  elseif info.image then
    w, h =info.image:getDimensions()
  end

  local align_x = info.align_x or 0.5
  local align_y = info.align_y or 1.0

  w = w * scale
  h = h * scale

  x = x - w * align_x
  y = y - h * align_y

  return x, y, scale, w, h
end



function draw_collision_trapezoids(ent)
  local info = ent.info

  if info.no_collide then return end

  local x, y, scale, w, h = transform_entity(ent)

  love.graphics.setColor(255, 0, 0)

  for _,T in ipairs(info.trapezoids) do
    for i = 1, #T - 1 do
      local TA = T[i]
      local TB = T[i + 1]

      local ax1 = x + TA.x1 * w
      local ax2 = x + TA.x2 * w
      local ay  = y + TA.y  * h

      local bx1 = x + TB.x1 * w
      local bx2 = x + TB.x2 * w
      local by  = y + TB.y  * h

      love.graphics.line(ax1, ay,  ax2, ay,  bx2, by,  bx1, by,  ax1, ay)
    end
  end
end



function calc_entity_lighting(ent)
  local y = ent.y

  -- make some objects brighter
  if ent.bright or ent.info.bright then
    y = y - 64 * 6
  end

  return transform_lighting(255, 255, 255, y)
end



function draw_entity(ent)
  local info = ent.info

  if ent.no_draw then return end

  -- skip when too close to camera
  if ent.y < Game.cam_y + 4 then
    return
  end

  local x, y, scale, w, h = transform_entity(ent)

  love.graphics.setColor(calc_entity_lighting(ent))

  if ent.is_hurt then
    love.graphics.setColor(255, 0, 0)
  end

  -- IDEA : mirroring

  -- the "null" entity has no image, useful for particles
  if ent.info.image then
    if ent.quad then
      love.graphics.draw(info.image, ent.quad, x, y, 0, scale, scale)
    else
      love.graphics.draw(info.image, x, y, 0, scale, scale)
    end
  end

  if ent.particles then
    draw_particle_group(ent, x + w * 0.5, y + h * 0.5, scale)
  end

  -- this is only for debugging
  ent.is_colliding = false

--  draw_collision_trapezoids(ent)
end



function draw_all_entities()

  table.sort(ActiveEnts,
      function(A, B) return A.y > B.y end)

  for i = 1, #ActiveEnts do
    local ent = ActiveEnts[i]

    if ent == Player and Player.dead then
      -- draw dead players after everything else
    else
      draw_entity(ent)
    end
  end

  if Player.dead then
    draw_entity(Player)
  end
end



function draw_background()
  local image = Gfx[Game.lev_info.background]
  assert(image)

  love.graphics.setColor(255, 255, 255)
  love.graphics.draw(image, 0, 0)
end



function draw_a_ground_square(x, y)
  local col = FLOOR_COLORS[Game.lev_info.floor_color]
  local r, g, b

  local is_outer = (math.abs(x) >= 4)

  if is_outer and Game.lev_info.outer_color then
    col = FLOOR_COLORS[Game.lev_info.outer_color]
  end

  -- make the grid pattern, every second square is darker
  if (x + y + 100) % 2 == 1 then
    r, g, b = col[1], col[2], col[3]
  else
    r, g, b = col[4], col[5], col[6]
  end

  -- make the horizontal edges fade off to black  [ DISABLED ]
  if false and is_outer and not Game.lev_info.outer_color then
    local k = 0.75

    r = r * k
    g = g * k
    b = b * k
  end

  local SZ = SQUARE_SZ

  x = (x - 0.5) * SZ
  y = y * SZ

  -- make squares in the distance be darker
  love.graphics.setColor(transform_lighting(r, g, b, y))

  local x1, y1 = transform_point(x,    y,   0)
  local x2, y2 = transform_point(x+SZ, y,   0)
  local x3, y3 = transform_point(x+SZ, y+SZ, 0)
  local x4, y4 = transform_point(x,    y+SZ, 0)

  love.graphics.polygon("fill", x1,y1, x2,y2, x3,y3, x4,y4)
end



function draw_ground()
  local first_y = math.floor(Game.cam_y / SQUARE_SZ)

  for x = -GROUND_W, GROUND_W + 1 do
  for y = 0, GROUND_H do
    draw_a_ground_square(x, first_y + y)
  end
  end
end


---=== PARTICLE SYSTEM ======------------------------------


function particle_effect(name, ex, ey, ez, num1, num2)
  local fx = PARTICLE_EFFECTS[name]
  if not fx then
    error("Unknown particle effect: " .. tostring(fx))
  end

  local defs = PARTICLE_EFFECTS.defaults
  assert(defs)

  -- select number of particles
  num1 = num1 or fx.num1 or defs.num1
  num2 = num2 or fx.num2 or num1

  local count = rand_int_range(num1, num2)

  if count <= 0 then return end

  local color1 = fx.color1 or defs.color1
  local color2 = fx.color2 or color1

  local offset1 = fx.offset1 or defs.offset1
  local offset2 = fx.offset2 or offset1

  local veloc1 = fx.veloc1 or defs.veloc1
  local veloc2 = fx.veloc2 or veloc1

  local size1  = fx.size1 or defs.size1
  local size2  = fx.size2 or size1

  local time1  = fx.time1 or defs.time1
  local time2  = fx.time2 or time1

  -- spawn a new entity
  local ent = entity_spawn_by_name("null", ex, ey, ez)

  ent.particles = {}

  for i = 1, count do
    local p = {}

    p.fx = fx

    p.r = rand_range(color1[1], color2[1])
    p.g = rand_range(color1[2], color2[2])
    p.b = rand_range(color1[3], color2[3])
    p.a = rand_range(color1[4] or 255, color2[4] or 255)

    p.x = rand_range(offset1[1], offset2[1])
    p.y = rand_range(offset1[2], offset2[2])

    p.mom_x = rand_range(veloc1[1], veloc2[1])
    p.mom_y = rand_range(veloc1[2], veloc2[2])

    p.size  = rand_range(size1, size2)
    p.time  = rand_range(time1, time2)

    if p.time > 0 and p.size > 1 and p.a > 0 then
      table.insert(ent.particles, p)
    end
  end

  return ent
end



function update_particle_group(ent)
  local dt = Game.dt

  for i = #ent.particles, 1, -1 do
    local p  = ent.particles[i]
    local fx = p.fx

    p.time = p.time - dt
    p.size = p.size - (fx.size_decay  or 0) * dt
    p.a    = p.a    - (fx.alpha_decay or 0) * dt

    -- has particle died?
    if p.time <= 0 or p.size <= 1 or p.a < 1 then
      table.remove(ent.particles, i)

    else
      p.x = p.x + p.mom_x * dt
      p.y = p.y + p.mom_y * dt

      if p.fx.decelerate then
        local dec_x = p.fx.decelerate[1] * dt
        local dec_y = p.fx.decelerate[2] * dt

        if p.mom_x > 0 then
          p.mom_x = math.max(0, p.mom_x - dec_x)
        else
          p.mom_x = math.min(0, p.mom_x + dec_x)
        end

        if p.mom_y > 0 then
          p.mom_y = math.max(0, p.mom_y - dec_y)
        else
          p.mom_y = math.min(0, p.mom_y + dec_y)
        end
      end

      if p.fx.gravity then
        p.mom_y = p.mom_y + 250 * p.fx.gravity * dt
      end
    end
  end

  if #ent.particles == 0 then
    ent.particles = nil

    -- if entity only existed to show particles, mark it as dead
    if ent.info.kind == "null" then
      ent.dead = true
    end
  end
end



function draw_particle_group(ent, ex, ey, e_scale)
  local width = Gfx.particle:getDimensions()

  for _,p in ipairs(ent.particles) do
    local scale = p.size / width
    local alpha = math.min(p.a, 255)

    local x = ex + p.x * e_scale
    local y = ey + p.y * e_scale

    love.graphics.setColor(p.r, p.g, p.b, alpha)
    love.graphics.draw(Gfx.particle, x, y, 0, scale, scale)
  end
end


---=== PLAYER STATS =======---------------------------------


function printf_shadow(text, x, y, width, align)
  local r, g, b = love.graphics.getColor()

  -- shadow is black
  love.graphics.setColor(0, 0, 0)

  love.graphics.printf(text, x - 1, y,     width, align)
  love.graphics.printf(text, x + 1, y,     width, align)
  love.graphics.printf(text, x + 1, y + 1, width, align)
  love.graphics.printf(text, x,     y + 1, width, align)
  love.graphics.printf(text, x,     y - 1, width, align)

  love.graphics.setColor(r, g, b)

  love.graphics.printf(text, x, y, width, align)
end


function draw_health_bar(x, y, perc, bar_img)
  love.graphics.setColor(255, 255, 255)
  love.graphics.draw(Gfx.ui_bar_base, x, y)

  if perc >= 1 then
    local scale = perc / 100

    love.graphics.setFont(Fonts.small)
    love.graphics.draw(bar_img, x+5, y+5, 0, scale, 1.0)

    love.graphics.setColor(255, 255, 255)
    love.graphics.printf(string.format("%d%%", perc), x, y + 4, 128, "center")
  end
end



function draw_boss_fight_stat()
  -- determine health of remaining bosses
  local cur   = 0
  local total = Game.boss_total_health

  if total <= 0 then return end

  for _,ent in ipairs(ActiveEnts) do
    if ent.info.kind == "boss" and not ent.dead then
      cur = cur + ent.health
    end
  end

  cur = math.ceil(100 * cur / total)

  love.graphics.setFont(Fonts.normal)
  love.graphics.setColor(16 , 16 , 16 )
  love.graphics.printf("Enemies:", 455, 60, 200, "right")

  draw_health_bar(668, 54, cur, Gfx.ui_bar_blue)
end



function draw_player_stats()
  love.graphics.setFont(Fonts.normal)
  love.graphics.setColor(16, 16, 16)
  love.graphics.print("Score:", 10, 10)

  love.graphics.setFont(Fonts.score)
  love.graphics.setColor(208, 208, 208)
  printf_shadow(string.format("%06d", Player.score), 80, 4, 800, "left")


  love.graphics.setFont(Fonts.normal)
  love.graphics.setColor(16 , 16 , 16 )
  love.graphics.printf("Health:", 455, 10, 200, "right")


  draw_health_bar(668, 4, math.ceil(Player.health), Gfx.ui_bar_red)


  if Game.boss_fight then
    draw_boss_fight_stat()
  end


  if Game.finished or
     -- game is about to finish...
     (Game.level_over and Game.level >= #LEVEL_LIST)
  then
    love.graphics.setFont(Fonts.title)
    love.graphics.setColor(255, 80, 40)

    if not Game.in_menu then
      if Player.dead then
        printf_shadow("Game Over", 0, 150, 800, "center")
      else
        love.graphics.setColor(255, 234, 40)
        printf_shadow("Congratulations!", 0, 130, 800, "center")
        printf_shadow("You Have Won!",    0, 170, 800, "center")

        local t = Game.time - Game.level_over

        if t >= 7 then
          local sc = string.format("$%d", Player.score * 10 + 200000)

          love.graphics.setFont(Fonts.score)
          love.graphics.setColor(192, 192, 192)
          printf_shadow("Cash Prize:", 230, 280, 800, "left")

          love.graphics.setFont(Fonts.title)
          love.graphics.setColor(255, 255, 255)
          printf_shadow(sc, 430, 280, 800, "left")

          local t_scale = 0.75
          love.graphics.draw(Gfx.ui_trophy, 400 - 128 * t_scale, 540 - 256 * t_scale, 0, t_scale, t_scale)
        end
      end
    end

  elseif Game.level_over then
    local t = Game.time - Game.level_over

    -- darken screen as we transition to next level
    if t >= 4 then
      local a = (t - 4) * 255 / 4
      if a > 255 then a = 255 end

      love.graphics.setColor(0, 0, 0, a)
      love.graphics.rectangle("fill", 0, 0, 800, 600)
    end

    if not Game.in_menu then
      love.graphics.setFont(Fonts.title)
      love.graphics.setColor(255, 80, 40)

      printf_shadow("Level Completed!", 0, 150, 800, "center")

      if t >= 3 then
        love.graphics.setFont(Fonts.normal)
        love.graphics.setColor(208, 208, 208)

        printf_shadow("Entering next level....", 0, 300, 800, "center")
      end
    end

  elseif Player.dead and not Player.dying then
    if not Game.in_menu then
      love.graphics.setFont(Fonts.title)
      love.graphics.setColor(255, 80, 40)

      if Game.lev_info.is_bonus then
        printf_shadow("Failed the Bonus level", 0, 150, 800, "center")
      elseif Game.enter_score >= NEW_LIFE_COST * 2 then
        printf_shadow("Lost a Life", 0, 150, 800, "center")
      else
        printf_shadow("Only One Life Left!", 0, 150, 800, "center")
      end

      love.graphics.setFont(Fonts.normal)
      love.graphics.setColor(208, 208, 208)

      if Game.lev_info.is_bonus then
        printf_shadow("press ENTER to play next level", 0, 300, 800, "center")
      else
        printf_shadow("press ENTER to restart level", 0, 300, 800, "center")
      end
    end
  
  elseif Game.lev_info.is_bonus then
    love.graphics.setFont(Fonts.title)
    love.graphics.setColor(128, 208, 128)

    printf_shadow("Bonus Level", 0, 40, 800, "center")
  end
end



---=== MENUS and SCREENS ======------------------------------


MENU_Y1 = 130

HINT_COLOR = { 255, 192, 128 }


function draw_title_pic()
  love.graphics.setColor(255, 255, 255)
  love.graphics.draw(Gfx.ui_title, 0, 0)
end



function draw_transparent_box(x1, y1, x2, y2)
  -- draw a transparent box with a light/dark border
  love.graphics.setColor(0, 0, 0, 160)
  love.graphics.rectangle("fill", x1, y1, x2 - x1, y2 - y1)

  love.graphics.setColor(128, 128, 128)
  love.graphics.line(x1, y2, x2, y2, x2, y1)

  love.graphics.setColor(192, 192, 192)
  love.graphics.line(x1, y2, x1, y1, x2, y1)
end



function draw_a_menu_line(idx, y, name)
  local is_highlighted = (idx == Game.menu_pos)

  love.graphics.setFont(Fonts.large)
  if is_highlighted then
    love.graphics.setColor(255, 255, 255)
  else
    love.graphics.setColor(0, 128, 255)
  end
  love.graphics.printf(name, 0, y, 800, "center")
end



function menu_draw_main_menu()
  local x1 = 200
  local y1 = MENU_Y1
  local x2 = 600
  local y2 = 520

  draw_transparent_box(x1, y1, x2, y2)

  y1 = y1 + 30

  love.graphics.setFont(Fonts.score)
  love.graphics.setColor(255, 192, 128)
  love.graphics.printf("MAIN MENU", 0, y1, 800, "center")

  -- draw each menu option
  y1 = y1 + 80

  draw_a_menu_line(1, y1 +   0, "Start Game")
  draw_a_menu_line(2, y1 +  40, "Options")
  draw_a_menu_line(3, y1 +  80, "Help")
  draw_a_menu_line(4, y1 + 120, "Credits")
  draw_a_menu_line(5, y1 + 180, "QUIT")

  -- small help at bottom
  love.graphics.setFont(Fonts.normal)
  love.graphics.setColor(HINT_COLOR)

  printf_shadow("Use cursor keys to navigate, ENTER to select", 0, 550, 800, "center")
end



function remove_word(text)
  local w, rest = string.match(text, "^(%s*%S+)%s*(.*)$")

  if not (w and rest) then
    return text, ""
  end

  -- add an extra space after the end of a sentence
  if #w > 1 and string.sub(w, #w, #w) == '.' then
    w = w .. " "
  end

  return w, rest
end



function wrap_paragraph(p, width, font)
  p.lines = {}

  local text = p.t

  for loop = 1, 20 do
    -- terminating condition : current line fits
    if font:getWidth(text) <= width then
      table.insert(p.lines, text)
      return
    end

    -- begin a new line, using the first word
    local cur_line

    cur_line, text = remove_word(text)

    -- add words to current line until we hit the width limit
    for pass = 1, 50 do
      local w, new_text = remove_word(text)

      if font:getWidth(cur_line .. " " .. w) > width then
        break;
      end

      cur_line = cur_line .. " " .. w
      text     = new_text

      if text == "" then break; end
    end

    table.insert(p.lines, cur_line)

    -- also stop when we run out of text
    if text == "" then
      return
    end
  end
end



function draw_a_paragraph(y, p)
  -- returns a new y coordinate

  assert(p.t)

  local font   = nil
  local line_h = 30

  if p.small then
    font = Fonts.small
  elseif p.large then
    font = Fonts.large
    line_h = 40
  elseif p.title then
    font = Fonts.title
    line_h = 50
  else
    font = Fonts.normal
  end

  love.graphics.setFont(font)

  if p.line_h then
    line_h = p.line_h
  end

  if p.color then
    love.graphics.setColor(p.color)
  elseif p.title then
    love.graphics.setColor(104, 160, 255)
  else
    love.graphics.setColor(208, 208, 208)
  end

  if p.center == 2 then
    local front = string.match(p.t, "^.*:") or ""
    local x = 400 - font:getWidth(front)

    love.graphics.print(p.t, x, y)
    return y + line_h

  elseif p.title or p.center then
    love.graphics.printf(p.t, 0, y, 800, "center")
    return y + line_h
  end

  -- split paragraphs into one or more lines
  if not p.lines then
    wrap_paragraph(p, 600, font)
  end

  for i = 1, #p.lines do
    love.graphics.print(p.lines[i], 100, y)
    y = y + line_h
  end

  return y
end



function menu_draw_help()
  local y = MENU_Y1

  draw_transparent_box(80, y, 720, 560)

  love.graphics.setScissor(85, y + 5, 630, 560 - y - 10)

  y = y + 50 - Game.help_pos * 30

  local start_y = y

  for i = 1, #Help do
    local s = Help[i]

    -- replace bare strings with a table
    if type(s) == "string" then
      s = { t=s }
      Help[i] = s
    end

    y = draw_a_paragraph(y, Help[i])

    y = y + (s.gap or 20)
  end

  -- use this to determine Help_height
  -- print(y - start_y)

  love.graphics.setScissor()

--[[
  love.graphics.setFont(Fonts.normal)
  love.graphics.setColor(HINT_COLOR)

  printf_shadow("Scroll text with cursor keys or PGUP / PGDN", 0, 540, 800, "center")
--]]
end



function menu_help_key(key)
  if key == "up" then
    Game.help_pos = Game.help_pos - 1

  elseif key == "down" then
    Game.help_pos = Game.help_pos + 1

  elseif key == "pageup" then
    Game.help_pos = Game.help_pos - 10

  elseif key == "pagedown" then
    Game.help_pos = Game.help_pos + 10
  end

  local max_pos = math.ceil(Help_height / 30) - 10

  if Game.help_pos < 1 then Game.help_pos = 1 end
  if Game.help_pos > max_pos then Game.help_pos = max_pos end
end



function draw_a_credit_line(y, info)
  love.graphics.setFont(Fonts.normal)

  if info.is_title then
    love.graphics.setFont(Fonts.title)
    love.graphics.setColor(104, 160, 255)

  elseif info.is_heading then
    love.graphics.setColor(0, 0, 255)

  else
    love.graphics.setColor(255, 255, 255)
  end

  love.graphics.print(info.what, 100, y)
  love.graphics.print(info.lic,  325, y)

  if #info.name > 18 then
    love.graphics.setFont(Fonts.small)
  end

  love.graphics.print(info.name, 505, y)
end



function menu_draw_credits()
  local y = MENU_Y1

  draw_transparent_box(80, y, 720, 560)

  love.graphics.setScissor(85, y + 5, 630, 560 - y - 10)

  -- draw all the credit lines, make them scroll up
  y = 450 - Game.menu_time * 80

  for i = 1, #Credits do
    draw_a_credit_line(y, Credits[i])

    y = y + 30
  end

  love.graphics.setScissor()

  -- once all credits are shown, reset to beginning
  if y < MENU_Y1 then
    Game.menu_time = 0
  end

--[[
  love.graphics.setFont(Fonts.normal)
  love.graphics.setColor(HINT_COLOR)

  printf_shadow("Press ESC or BACKSPACE to return to menu", 0, 540, 800, "center")
--]]
end



function draw_a_slider(x, y, along)
  x = x - 10

  love.graphics.draw(Gfx.ui_slider, x, y - 15)

  x = x + along * 190

  love.graphics.draw(Gfx.ui_knob, x, y - 5)
end



function draw_an_option(idx, y, name)
  local is_highlighted = (idx == Game.option_pos)

  love.graphics.setFont(Fonts.large)
  if is_highlighted then
    love.graphics.setColor(255, 255, 255)
  else
    love.graphics.setColor(0, 128, 255)
  end
  love.graphics.print(name, 200, y)

  love.graphics.setColor(208, 208, 208)

  local x = 430

  if idx == 1 then
    draw_a_slider(x, y, Options.sound_volume)

  elseif idx == 2 then
    local what = "Off"
    if Options.music_volume > 0.4 then what = "Quiet" end
    if Options.music_volume > 0.7 then what = "ON" end

    love.graphics.print(what, x, y)

  elseif idx == 3 then
    local what = "Off"
    if Options.fullscreen then what = "ON" end

    love.graphics.print(what, x, y)
  end
end



function menu_draw_options()
  draw_transparent_box(100, MENU_Y1, 700, 500)

  local y1 = MENU_Y1 + 30

  love.graphics.setFont(Fonts.title)
  love.graphics.setColor(104, 160, 255)
  love.graphics.printf("OPTIONS", 0, y1, 800, "center")

  y1 = y1 + 80

  draw_an_option(1, y1 +   0, "Sound Volume")
  draw_an_option(2, y1 +  50, "Music Mode")
  draw_an_option(3, y1 + 100, "Full-Screen")

  love.graphics.setFont(Fonts.normal)
  love.graphics.setColor(HINT_COLOR)

  printf_shadow("Use cursor keys to navigate and change options", 0, 540, 800, "center")
end



function bump_sound_volume(dir)
  local new_val = Options.sound_volume

  new_val = new_val + dir * 0.1

  if new_val < 0 then new_val = 0 end
  if new_val > 1 then new_val = 1 end

  if Options.sound_volume ~= new_val then
     Options.sound_volume =  new_val
     update_sound_volume()
  end
end



function bump_music_volume(dir)
  local new_val = Options.music_volume

  if dir == "toggle" then
    if Options.music_volume >= 1 then
       new_val = 0
    else
       new_val = 1
    end

  else
    new_val = new_val + dir

    if new_val < 0 then new_val = 0 end
    if new_val > 1 then new_val = 1 end
  end

  if Options.music_volume ~= new_val then
     Options.music_volume =  new_val
     update_music_volume()
  end
end



function bump_fullscreen(dir)
  local new_val = Options.fullscreen

  if dir == "toggle" then
    new_val = not new_val
  elseif dir > 0 then
    new_val = true
  elseif dir < 0 then
    new_val = false
  end

  if Options.fullscreen ~= new_val then
     Options.fullscreen =  new_val
     update_fullscreen()
  end
end



function menu_options_key(key)
  if key == "enter" or key == "return" then
    if Game.option_pos == 2 then key = 'm' end
    if Game.option_pos == 3 then key = 'f' end
  end

  if key == "s" then
    Game.option_pos = 1

  elseif key == "m" then
    Game.option_pos = 2
    bump_music_volume("toggle")

  elseif key == "f" then
    Game.option_pos = 3
    bump_fullscreen("toggle")

  elseif key == "up" then
    if Game.option_pos > 1 then
       Game.option_pos = Game.option_pos - 1
    end

  elseif key == "down" then
    if Game.option_pos < 3 then
       Game.option_pos = Game.option_pos + 1
    end

  elseif key == "left" then
    if Game.option_pos == 1 then bump_sound_volume(-1) end
    if Game.option_pos == 2 then bump_music_volume(-1) end
    if Game.option_pos == 3 then bump_fullscreen(-1) end

  elseif key == "right" then
    if Game.option_pos == 1 then bump_sound_volume(1) end
    if Game.option_pos == 2 then bump_music_volume(1) end
    if Game.option_pos == 3 then bump_fullscreen(1) end
  end
end



function menu_draw()
  if Game.screen == "options" then
    menu_draw_options()

  elseif Game.screen == "help" then
    menu_draw_help()

  elseif Game.screen == "credits" then
    menu_draw_credits()

  else
    menu_draw_main_menu()
  end
end



function update_fullscreen()
  if Options.fullscreen then
    love.window.setFullscreen(true)
    love.mouse.setVisible(false)
  else
    love.window.setFullscreen(false)
    love.mouse.setVisible(true)
  end
end



function menu_key_pressed(key)

  -- handle the special screens

  if Game.screen then
    -- return to main menu?
    if key == "backspace" or key == "escape" or
       key == "o" or key == "h" or key == "c"
    then
      -- when coming back from options screen, save the settings
      if Game.screen == "options" then
        save_user_settings()
      end

      Game.screen = nil
      return
    end

    if Game.screen == "options" then
      menu_options_key(key)
    elseif Game.screen == "help" then
      menu_help_key(key)
    end

    return
  end

  -- handle the main menu

  -- hide the main menu during a game?
  if true then --??  Game.active then
    if key == "escape" or key == "backspace" then
      Game.in_menu = false
      return
    end
  end

  -- handle activating the currently selected choice
  if key == "enter" or key == "return" then
    if Game.menu_pos == 1 then key = 's' end
    if Game.menu_pos == 2 then key = 'o' end
    if Game.menu_pos == 3 then key = 'h' end
    if Game.menu_pos == 4 then key = 'c' end
    if Game.menu_pos == 5 then key = 'q' end
  end

  if key == "s" and (not Game.active or Game.finished) then
    game_start_new()

  elseif key == "o" then
    Game.screen = "options"

  elseif key == "h" then
    Game.screen = "help"

  elseif key == "c" then
    Game.screen = "credits"

    -- this is used to scroll credits
    Game.menu_time = 0

  elseif key == "q" then
    game_quit()

  elseif key == "f" then
    Options.fullscreen = not Options.fullscreen
    update_fullscreen()

  elseif key == "up" then
    local min_pos = 1
    if Game.active and not Game.finished then min_pos = 2 end

    if Game.menu_pos > min_pos then
       Game.menu_pos = Game.menu_pos - 1
    end

  elseif key == "down" then
    if Game.menu_pos < 5 then
       Game.menu_pos = Game.menu_pos + 1
    end
  end
end

