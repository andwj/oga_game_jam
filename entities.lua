--
--  Entity definitions
--
--  Copyright (C) 2017  Andrew Apted
--
--  Under the GNU General Public License (GPL), version 3 or
--  (at your option) any later version.
--

ENTITY_TYPES =
{
  -- players --

  player =
  {
    kind = "player",

    mass = 100,
    falls = true,
    death_gravity = true,

    health = 100,

    image_file = "pl_player.png",
    tile_size  = { 5, 5 },
    scale      = 0.5,
    align_y    = 0.8,

    trapezoids =
    {
      {
        { x1=0.44, x2=0.53, y=0.20 },
        { x1=0.33, x2=0.65, y=0.35 },
        { x1=0.40, x2=0.57, y=0.80 },
      },
    },
  },

  -- missiles and miscellany --

  null =
  {
    -- this type has no image, mainly used for particles
    kind = "fx",

    mass = 50,
    no_friction = true,
    no_collide = true,

    scale = 1,

    trapezoids = {},
  },

  bomb =
  {
    kind = "missile",

    mass = 50,
    falls = true,
    bounces = true,
    no_friction = true,

    damage = 50,
    life_time = 2.1,

    image_file = "fx_bomb.png",
    scale      = 0.40,
    align_y    = 0.56,

    sound_file = "pl_shoot.wav",
    max_poly   = 3,

    trapezoids =
    {
      {
        { x1=0.46, x2=0.57, y=0.30 },
        { x1=0.31, x2=0.73, y=0.40 },
        { x1=0.26, x2=0.78, y=0.56 },
        { x1=0.31, x2=0.73, y=0.71 },
        { x1=0.46, x2=0.57, y=0.82 },
      },
    },
  },

  red_bomb =
  {
    kind = "missile",

    mass = 50,
    falls = true,
    bounces = true,
    no_friction = true,

    damage = 80,
    life_time = 6.1,

    image_file = "fx_bomb2.png",
    scale      = 0.40,
    align_y    = 0.56,

    trapezoids =
    {
      {
        { x1=0.43, x2=0.54, y=0.30 },
        { x1=0.27, x2=0.69, y=0.40 },
        { x1=0.22, x2=0.74, y=0.56 },
        { x1=0.27, x2=0.69, y=0.71 },
        { x1=0.43, x2=0.54, y=0.82 },
      },
    },
  },

  explosion =
  {
    kind = "fx",

    mass = 20,
    no_friction = true,
    no_collide  = true,

    image_file = "fx_explosion.png",
    tile_size  = { 10, 1 },
    scale      = 1.33,
    align_y    = 0.5,

    bright = true,
    anim_rate = 16.0,
    one_shot = true,

    trapezoids = {},
  },

  laser_beam =
  {
    kind = "missile",

    mass = 40,
    no_friction = true,

    damage = 30,

    image_file = "fx_beams.png",
    tile_size  = { 3, 1 },
    scale      = 1.00,
    align_y    = 0.75,
    bright     = true,

    trapezoids =
    {
      {
        { x1=0.44, x2=0.48, y=0.32 },
        { x1=0.37, x2=0.54, y=0.56 },
        { x1=0.26, x2=0.65, y=0.70 },
        { x1=0.26, x2=0.65, y=0.78 },
        { x1=0.38, x2=0.54, y=0.88 },
      },
    },
  },

  spiked_ball =
  {
    kind = "missile",

    mass = 40,
    falls   = true,
    bounces = true,
    no_friction = true,

    damage = 66,

    image_file = "fx_spiked_ball.png",
    scale      = 0.70,
    align_y    = 0.5,

    trapezoids =
    {
      {
        { x1=0.47, x2=0.48, y=0.01 },
        { x1=0.45, x2=0.50, y=0.09 },
        { x1=0.20, x2=0.78, y=0.22 },
        { x1=0.11, x2=0.86, y=0.45 },
        { x1=0.01, x2=0.99, y=0.49 },
        { x1=0.11, x2=0.86, y=0.54 },
        { x1=0.21, x2=0.76, y=0.76 },
        { x1=0.44, x2=0.52, y=0.86 },
        { x1=0.47, x2=0.48, y=0.99 },
      },

      {
        { x1=0.15, x2=0.17, y=0.09 },
        { x1=0.26, x2=0.41, y=0.27 },
      },

      {
        { x1=0.77, x2=0.79, y=0.08 },
        { x1=0.56, x2=0.72, y=0.27 },
      },

      {
        { x1=0.24, x2=0.39, y=0.69 },
        { x1=0.14, x2=0.16, y=0.88 },
      },

      {
        { x1=0.59, x2=0.74, y=0.69 },
        { x1=0.80, x2=0.82, y=0.89 },
      },
    },
  },

  dragon_flame =
  {
    kind = "missile",

    mass = 10,
    no_friction = true,

    damage = 10,

    image_file = "fx_dragon_flame.png",
    tile_size  = { 16, 1 },
    scale      = 3.00,
    align_y    = 0.05,

    anim_rate  = 12.0,
    bright     = true,

    trapezoids =
    {
      {
        { x1=0.25, x2=0.75, y=0.00 },
        { x1=0.25, x2=0.75, y=0.75 },
      },
    },
  },

  jump_pad =
  {
    kind = "jump_pad",
    ed_num = 404,
    ed_name = "Jump Pad",

    mass = 860,

    image_file = "jump_pad.png",
    tile_size  = { 3,1 },
    scale      = 0.4,
    align_y    = 0.4,

    anim_rate  = 12,
    bright     = true,

    trapezoids =
    {
      {
        { x1=0.01, x2=0.99, y=0.01 },
        { x1=0.01, x2=0.99, y=0.99 },
      },
    },
  },

  -- items --

  coin =
  {
    kind = "item",
    ed_num = 201,
    ed_name = "Coin",

    mass = 20,

    image_file = "item_coins.png",
    tile_size  = { 6, 1 },
    scale      = 0.33,
    align_y    = 0.5,

    anim_rate = 12.0,
    bright = true,

    sound_file = "item_coin.wav",
    max_poly   = 3,

    add_score  = 50,

    trapezoids =
    {
      {
        { x1=0.30, x2=0.70, y=0.00 },
        { x1=0.00, x2=1.00, y=0.30 },
        { x1=0.00, x2=1.00, y=0.70 },
        { x1=0.30, x2=0.70, y=1.00 },
      },
    },
  },

  gem =
  {
    kind = "item",
    ed_num = 202,
    ed_name = "Red Gem",

    mass = 40,

    image_file = "item_gem.png",
    scale      = 0.40,
    align_y    = 0.5,

    bright = true,

    sound_file = "item_gem.wav",
    max_poly   = 2,

    add_score  = 500,

    trapezoids =
    {
      {
        { x1=0.36, x2=0.56, y=0.18 },
        { x1=0.12, x2=0.90, y=0.38 },
        { x1=0.18, x2=0.83, y=0.58 },
        { x1=0.47, x2=0.55, y=0.88 },
      },
    },
  },

  apple =
  {
    kind = "item",
    ed_num = 211,
    ed_name = "Health Apple",

    mass = 10,

    image_file = "item_apple.png",
    scale      = 0.30,
    align_y    = 0.5,

    add_health = 5,

    trapezoids =
    {
      {
        { x1=0.01, x2=0.99, y=0.00 },
        { x1=0.01, x2=0.99, y=1.00 },
      },
    },
  },

  banana =
  {
    kind = "item",
    ed_num = 212,
    ed_name = "Health Banana",

    mass = 10,

    image_file = "item_banana.png",
    scale      = 0.27,
    align_y    = 0.5,

    add_health = 5,

    trapezoids =
    {
      {
        { x1=0.01, x2=0.99, y=0.00 },
        { x1=0.01, x2=0.99, y=1.00 },
      },
    },
  },

  potion =
  {
    kind = "item",
    ed_num = 215,
    ed_name = "Health Potion",

    mass = 40,

    image_file = "item_potion1.png",
    scale      = 0.40,
    align_y    = 0.5,

    bright = true,

    sound_file = "item_potion.wav",
    max_poly   = 1,

    add_health = 100,

    trapezoids =
    {
      {
        { x1=0.35, x2=0.65, y=0.00 },
        { x1=0.35, x2=0.65, y=0.28 },
        { x1=0.11, x2=0.89, y=0.44 },
        { x1=0.11, x2=0.89, y=0.80 },
        { x1=0.29, x2=0.71, y=1.00 },
      },
    },
  },

  -- trees --

  tree1 =
  {
    kind = "obstacle",
    ed_num = 105,
    ed_name = "Tree 1",

    mass = 260,
    health = 20,

    image_file = "ob_tree1.png",
    scale      = 1.4,

    trapezoids =
    {
      {
        { x1=0.47, x2=0.63, y=0.02 },
        { x1=0.20, x2=0.85, y=0.10 },
        { x1=0.15, x2=0.89, y=0.24 },
        { x1=0.22, x2=0.92, y=0.35 },
        { x1=0.28, x2=0.69, y=0.46 },
        { x1=0.47, x2=0.54, y=0.60 },
        { x1=0.35, x2=0.54, y=0.99 },
      },

    },
  },

  tree2 =
  {
    kind = "obstacle",
    ed_num = 106,
    ed_name = "Tree 2",

    mass = 200,
    health = 20,

    image_file = "ob_tree2.png",
    scale      = 1.0,

    trapezoids =
    {
      {
        { x1=0.43, x2=0.52, y=0.57 },
        { x1=0.39, x2=0.52, y=0.85 },
        { x1=0.24, x2=0.48, y=0.99 },
      },

      {
        { x1=0.54, x2=0.67, y=0.04 },
        { x1=0.08, x2=0.74, y=0.21 },
        { x1=0.23, x2=0.84, y=0.43 },
        { x1=0.23, x2=0.60, y=0.49 },
        { x1=0.33, x2=0.52, y=0.57 },
      },
    },
  },

  tree3 =
  {
    kind = "obstacle",
    ed_num = 107,
    ed_name = "Tree 3",

    mass = 200,
    health = 20,

    image_file = "ob_tree3.png",
    scale      = 1.0,

    trapezoids =
    {
      {
        { x1=0.53, x2=0.67, y=0.01 },
        { x1=0.46, x2=0.91, y=0.16 },
        { x1=0.16, x2=0.89, y=0.21 },
        { x1=0.20, x2=0.83, y=0.47 },
        { x1=0.56, x2=0.58, y=0.57 },
        { x1=0.36, x2=0.60, y=0.99 },
      },
    },
  },

  bushy_tree =
  {
    kind = "obstacle",
    ed_num = 108,
    ed_name = "Bushy Tree",

    mass = 260,
    health = 20,

    image_file = "ob_tree4.png",
    scale      = 1.0,
    align_y    = 0.95,

    trapezoids =
    {
      {
        { x1=0.46, x2=0.66, y=0.03 },
        { x1=0.19, x2=0.82, y=0.21 },
        { x1=0.08, x2=0.89, y=0.36 },
        { x1=0.04, x2=0.92, y=0.52 },
        { x1=0.10, x2=0.75, y=0.69 },
        { x1=0.46, x2=0.52, y=0.78 },
        { x1=0.39, x2=0.56, y=0.96 },
      },

    },
  },

  cherry_tree =
  {
    kind = "obstacle",
    ed_num = 109,
    ed_name = "Cherry Tree",

    mass = 260,
    health = 20,

    image_file = "ob_cherrytree.png",
    scale      = 0.6,

    trapezoids =
    {
      {
        { x1=0.52, x2=0.68, y=0.09 },
        { x1=0.49, x2=0.86, y=0.19 },
        { x1=0.07, x2=0.93, y=0.25 },
        { x1=0.19, x2=0.66, y=0.49 },
        { x1=0.25, x2=0.84, y=0.52 },
        { x1=0.53, x2=0.74, y=0.61 },
        { x1=0.54, x2=0.60, y=0.75 },
        { x1=0.48, x2=0.60, y=0.97 },
      },

    },
  },

  dead_tree =
  {
    kind = "obstacle",
    ed_num = 110,
    ed_name = "Dead Tree",

    mass = 260,
    health = 20,

    image_file = "ob_deadtree.png",
    scale      = 0.7,

    trapezoids =
    {
      {
        { x1=0.51, x2=0.53, y=0.05 },
        { x1=0.51, x2=0.56, y=0.36 },
        { x1=0.47, x2=0.57, y=0.44 },
        { x1=0.47, x2=0.57, y=0.72 },
        { x1=0.39, x2=0.63, y=0.98 },
      },

      {
        { x1=0.13, x2=0.19, y=0.44 },
        { x1=0.33, x2=0.48, y=0.58 },
        { x1=0.47, x2=0.50, y=0.71 },
      },

      {
        { x1=0.75, x2=0.95, y=0.25 },
        { x1=0.72, x2=0.82, y=0.31 },
        { x1=0.56, x2=0.73, y=0.35 },
        { x1=0.56, x2=0.59, y=0.42 },
      },
    },
  },

  palm_tree =
  {
    kind = "obstacle",
    ed_num = 120,
    ed_name = "Palm Tree",

    mass = 260,
    health = 20,

    image_file = "ob_palmtree.png",
    scale      = 0.7,

    trapezoids =
    {
      {
        { x1=0.39, x2=0.64, y=0.09 },
        { x1=0.34, x2=0.74, y=0.20 },
        { x1=0.54, x2=0.65, y=0.33 },
        { x1=0.65, x2=0.74, y=0.57 },
        { x1=0.60, x2=0.75, y=0.99 },
      },
    },
  },

  snow_pine1 =
  {
    kind = "obstacle",
    ed_num = 121,
    ed_name = "Snow Pine 1",

    mass = 260,
    health = 20,

    image_file = "snow_pine1.png",
    scale      = 1.2,

    trapezoids =
    {
      {
        { x1=0.44, x2=0.50, y=0.11 },
        { x1=0.30, x2=0.67, y=0.40 },
        { x1=0.15, x2=0.78, y=0.78 },
        { x1=0.45, x2=0.54, y=0.87 },
        { x1=0.44, x2=0.55, y=1.00 },
      },
    },
  },

  snow_pine2 =
  {
    kind = "obstacle",
    ed_num = 122,
    ed_name = "Snow Pine 2",

    mass = 260,
    health = 20,

    image_file = "snow_pine2.png",
    scale      = 1.5,

    trapezoids =
    {
      {
        { x1=0.46, x2=0.51, y=0.11 },
        { x1=0.32, x2=0.68, y=0.43 },
        { x1=0.17, x2=0.82, y=0.80 },
        { x1=0.45, x2=0.52, y=0.89 },
        { x1=0.44, x2=0.53, y=1.00 },
      },
    },
  },

  cactus1 =
  {
    kind = "obstacle",
    ed_num = 119,
    ed_name = "Cactus 1",

    mass = 100,
    health = 30,

    image_file = "ob_cactus1.png",
    scale      = 1.2,
    align_y    = 0.95,

    trapezoids =
    {
      {
        { x1=0.33, x2=0.56, y=0.02 },
        { x1=0.28, x2=0.59, y=0.53 },
        { x1=0.29, x2=0.68, y=0.81 },
        { x1=0.31, x2=0.63, y=0.99 },
      },

      {
        { x1=0.03, x2=0.21, y=0.19 },
        { x1=0.18, x2=0.39, y=0.36 },
      },

      {
        { x1=0.75, x2=0.94, y=0.43 },
        { x1=0.50, x2=0.78, y=0.63 },
      },
    },
  },

  -- obstacles --

  barrel =
  {
    kind = "obstacle",
    ed_num = 112,
    ed_name = "Barrel",

    mass = 100,
    health = 5,

    image_file = "ob_barrel.png",
    scale      = 0.7,

    trapezoids =
    {
      {
        { x1=0.16, x2=0.83, y=0.01 },
        { x1=0.02, x2=0.98, y=0.28 },
        { x1=0.02, x2=0.98, y=0.72 },
        { x1=0.16, x2=0.83, y=0.99 },
      },
    },
  },

  column =
  {
    kind = "obstacle",
    ed_num = 111,
    ed_name = "Greek Column",

    mass = 300,
    health = 10,

    image_file = "ob_column.png",
    scale      = 0.70,

    trapezoids =
    {
      {
        { x1=0.26, x2=0.73, y=0.01 },
        { x1=0.26, x2=0.73, y=0.06 },
        { x1=0.35, x2=0.64, y=0.11 },
        { x1=0.35, x2=0.64, y=0.88 },
        { x1=0.26, x2=0.73, y=0.94 },
        { x1=0.26, x2=0.73, y=0.99 },
      },
    },
  },

  wooden_crate =
  {
    kind = "obstacle",
    ed_num = 101,
    ed_name = "Wooden Crate",

    mass = 100,
    health = 10,

    image_file = "ob_crate1.png",
    scale      = 0.50,

    trapezoids =
    {
      {
        { x1=0.01, x2=0.99, y=0.01 },
        { x1=0.01, x2=0.99, y=0.99 },
      },
    },
  },

  metal_crate =
  {
    kind = "obstacle",
    ed_num = 102,
    ed_name = "Metal Crate",

    mass = 2000,
    -- no health, indestructible

    image_file = "ob_crate2.png",
    scale      = 0.50,

    trapezoids =
    {
      {
        { x1=0.01, x2=0.99, y=0.01 },
        { x1=0.01, x2=0.99, y=0.99 },
      },
    },
  },

  igloo =
  {
    kind = "obstacle",
    ed_num = 114,
    ed_name = "Igloo",

    mass = 150,
    health = 40,

    image_file = "ob_igloo.png",
    scale      = 0.90,
    align_y    = 0.90,

    trapezoids =
    {
      {
        { x1=0.39, x2=0.60, y=0.01 },
        { x1=0.14, x2=0.85, y=0.14 },
        { x1=0.01, x2=0.99, y=0.39 },
        { x1=0.01, x2=0.99, y=0.82 },
        { x1=0.28, x2=0.73, y=0.99 },
      },
    },
  },

  snowman =
  {
    kind = "obstacle",
    ed_num = 115,
    ed_name = "Snowman",

    mass = 100,
    health = 21,

    image_file = "snow_man.png",
    scale      = 1.00,
    align_y    = 0.90,

    trapezoids =
    {
      {
        { x1=0.52, x2=0.56, y=0.16 },
        { x1=0.52, x2=0.68, y=0.22 },
        { x1=0.28, x2=0.62, y=0.32 },
        { x1=0.31, x2=0.60, y=0.38 },
        { x1=0.33, x2=0.80, y=0.46 },
        { x1=0.07, x2=0.75, y=0.54 },
        { x1=0.16, x2=0.60, y=0.62 },
        { x1=0.13, x2=0.60, y=0.67 },
        { x1=0.29, x2=0.59, y=0.74 },
        { x1=0.26, x2=0.67, y=0.85 },
        { x1=0.29, x2=0.65, y=0.99 },
      },
    },
  },

  wooden_fence =
  {
    kind = "obstacle",
    ed_num = 116,
    ed_name = "Wooden Fence",

    mass = 60,
    health = 12,

    image_file = "ob_fence.png",
    scale      = 0.70,
    align_y    = 0.99,

    trapezoids =
    {
      {
        { x1=0.08, x2=0.92, y=0.05 },
        { x1=0.08, x2=0.92, y=1.00 },
      },
    },
  },

  icicle1 =
  {
    kind = "obstacle",
    ed_num = 118,
    ed_name = "Icicle 1",

    mass = 80,
    health = 14,

    image_file = "ob_icicle1.png",
    scale      = 1.60,
    align_y    = 0.87,

    trapezoids =
    {
      {
        { x1=0.45, x2=0.49, y=0.01 },
        { x1=0.40, x2=0.56, y=0.30 },
        { x1=0.32, x2=0.65, y=0.54 },
        { x1=0.22, x2=0.76, y=0.73 },
        { x1=0.17, x2=0.79, y=0.90 },
      },
    },
  },

  rock1 =
  {
    kind = "obstacle",
    ed_num = 117,
    ed_name = "Rock 1",

    mass = 60,
    health = 8,

    image_file = "ob_rock1.png",
    scale      = 1.70,
    align_y    = 0.84,

    trapezoids =
    {
      {
        { x1=0.43, x2=0.55, y=0.01 },
        { x1=0.24, x2=0.78, y=0.31 },
        { x1=0.07, x2=0.93, y=0.70 },
        { x1=0.16, x2=0.93, y=0.79 },
        { x1=0.35, x2=0.52, y=0.93 },
      },
    },
  },

  spikes_up =
  {
    kind = "obstacle",
    ed_num = 131,
    ed_name = "Spikes Up",

    mass = 200,
    insta_kill = true,

    image_file = "ob_spikes_up.png",
    scale      = 1.0,
    align_y    = 1.0,

    trapezoids =
    {
      {
        { x1=0.13, x2=0.82, y=0.44 },
        { x1=0.02, x2=0.93, y=0.69 },
        { x1=0.02, x2=0.95, y=0.97 },
      },
    },
  },

  spikes_down =
  {
    kind = "obstacle",
    ed_num = 132,
    ed_name = "Spikes Down",

    mass = 200,
    insta_kill = true,

    image_file = "ob_spikes_down.png",
    scale      = 1.0,
    align_y    = 0.0,

    trapezoids =
    {
      {
        { x1=0.02, x2=0.97, y=0.02 },
        { x1=0.05, x2=0.97, y=0.29 },
        { x1=0.18, x2=0.86, y=0.56 },
      },
    },
  },

  spikes_right =
  {
    kind = "obstacle",
    ed_num = 133,
    ed_name = "Spikes Right",

    mass = 200,
    insta_kill = true,

    image_file = "ob_spikes_right.png",
    scale      = 1.0,
    align_x    = 0.0,
    align_y    = 1.0,

    trapezoids =
    {
      {
        { x1=0.02, x2=0.29, y=0.05 },
        { x1=0.02, x2=0.56, y=0.18 },
        { x1=0.02, x2=0.56, y=0.87 },
        { x1=0.02, x2=0.29, y=0.96 },
      },
    },
  },

  spikes_left =
  {
    kind = "obstacle",
    ed_num = 134,
    ed_name = "Spikes Left",

    mass = 200,
    insta_kill = true,

    image_file = "ob_spikes_left.png",
    scale      = 1.0,
    align_x    = 1.0,
    align_y    = 1.0,

    trapezoids =
    {
      {
        { x1=0.70, x2=0.97, y=0.02 },
        { x1=0.42, x2=0.97, y=0.13 },
        { x1=0.42, x2=0.97, y=0.80 },
        { x1=0.70, x2=0.97, y=0.93 },
      },
    },
  },

  -- decorations --

--[[
  grass =
  {
    kind = "decoration",

    mass = 100,
    no_collide = true,

    image_file = "dec_grass2.png",
    scale      = 0.50,

    trapezoids = {},
  },
--]]

  -- static enemies --

  beetle =
  {
    kind = "enemy",
    ed_num = 301,
    ed_name = "Beetle",

    mass = 20,
    health = 20,

    image_file = "mon_beetle.png",
    tile_size  = { 5, 4 },
    scale      = 2.0,
    align_y    = 0.7,

    anim_rate = 8.0,

    trapezoids =
    {
      {
        { x1=0.45, x2=0.62, y=0.28 },
        { x1=0.26, x2=0.80, y=0.57 },
        { x1=0.36, x2=0.68, y=0.82 },
      },
    },
  },

  scorpion =
  {
    kind = "enemy",
    ed_num = 302,
    ed_name = "Scorpion",

    mass = 20,
    health = 75,

    image_file = "mon_scorpion.png",
    tile_size  = { 2, 1 },
    scale      = 2.0,
    align_y    = 0.75,

    anim_rate  = 5.0,

    trapezoids =
    {
      {
        { x1=0.37, x2=0.47, y=0.04 },
        { x1=0.27, x2=0.54, y=0.11 },
        { x1=0.30, x2=0.55, y=0.33 },
        { x1=0.30, x2=0.79, y=0.36 },
        { x1=0.09, x2=0.90, y=0.46 },
        { x1=0.15, x2=0.86, y=0.71 },
        { x1=0.43, x2=0.55, y=0.84 },
      },
    },
  },

  cobra =
  {
    kind = "enemy",
    ed_num = 303,
    ed_name = "Cobra",

    mass = 20,
    health = 50,

    image_file = "mon_cobra.png",
    tile_size  = { 3, 3 },
    scale      = 1.0,
    align_y    = 0.75,

    anim_rate  = 5.0,

    trapezoids =
    {
      {
        { x1=0.25, x2=0.75, y=0.02 },
        { x1=0.25, x2=0.75, y=0.99 },
      },
    },
  },

  eye_ball =
  {
    kind = "enemy",
    ed_num = 305,
    ed_name = "Eye Ball",

    mass = 70,
    health = 10,

    image_file = "mon_eye.png",
    scale      = 1.90,
    align_y    = 0.50,

    trapezoids =  -- FIXME
    {
      {
        { x1=0.19, x2=0.80, y=0.01 },
        { x1=0.18, x2=0.83, y=0.99 },
      },
    },
  },

  -- bosses --

  bat =
  {
    kind = "boss",
    movement = "bat",

    mass = 20,
    health = 55,

    image_file = "mon_bat.png",
    tile_size  = { 4, 1 },
    scale      = 1.50,
    align_y    = 0.5,

    anim_rate = 8.0,

    sounds =
    {
      pain_file = "bat_death.wav",
    },

    trapezoids =
    {
      {
        { x1=0.29, x2=0.71, y=0.24 },
        { x1=0.36, x2=0.64, y=0.62 },
      },
    },
  },

  dragon =
  {
    kind = "boss",
    movement = "dragon",

    mass = 200,
    health = 490,

    image_file = "mon_dragon.png",
    tile_size  = { 5, 1 },
    scale      = 2.1,
    align_y    = 0.5,

    anim_rate = 9.0,

    sounds =
    {
      pain_file = "dragon_pain.wav",
    },

    trapezoids =
    {
      {
        { x1=0.40, x2=0.60, y=0.24 },
        { x1=0.36, x2=0.64, y=0.62 },
      },
    },
  },

  creeper =
  {
    kind = "boss",
    movement = "creeper",

    mass = 50,
    health = 1290,
    no_friction = true,

    image_file = "mon_creeper.png",
    scale      = 2.00,
    align_y    = 0.5,

    sounds =
    {
      pain_file  = "creeper_pain.wav",
      shoot_file = "creeper_shoot.wav",
      laugh_file = "creeper_laugh.wav",
    },

    trapezoids =
    {
      {
        { x1=0.30, x2=0.66, y=0.29 },
        { x1=0.13, x2=0.84, y=0.50 },
        { x1=0.13, x2=0.84, y=0.71 },
        { x1=0.30, x2=0.66, y=0.91 },
      },
    },
  },

  creeper_body =
  {
    kind = "body",

    mass = 30,
    no_friction = true,

    image_file = "mon_creep_body.png",
    scale      = 1.50,
    align_y    = 0.5,

--  sound_file = "bat_death.wav",

    trapezoids =
    {
      {
        { x1=0.30, x2=0.66, y=0.29 },
        { x1=0.13, x2=0.84, y=0.50 },
        { x1=0.13, x2=0.84, y=0.71 },
        { x1=0.30, x2=0.66, y=0.91 },
      },
    },
  },

  penguin =
  {
    kind = "boss",
    movement = "bat",

    mass = 50,
    health = 40,

    image_file = "mon_penguin.png",
    tile_size  = { 2, 1 },
    scale      = 1.20,
    align_y    = 0.90,

    anim_rate  = 6,

    trapezoids =  -- FIXME
    {
      {
        { x1=0.19, x2=0.80, y=0.01 },
        { x1=0.18, x2=0.83, y=0.99 },
      },
    },
  },

}


--
-- this converts ed_num values to the ENTITY_TYPE entry.
-- call build_entity_map() to create it.
--
ENTITY_MAP = {}


function build_entity_map()
  for name,info in pairs(ENTITY_TYPES) do
    info.name = name

    if info.ed_num then
      -- sanity check
      if ENTITY_TYPES[info.ed_num] then
        error("Two entities using the same ed_num!")
      end

      ENTITY_MAP[info.ed_num] = info
    end
  end
end


--
-- this code creates info for the Eureka map editor
--
function build_eureka_config()
  -- sort the entries by ed_num
  local list = {}

  for _,info in pairs(ENTITY_TYPES) do
    if info.ed_num then
      table.insert(list, info)
    end
  end

  table.sort(list, function(A, B) return A.ed_num < B.ed_num end)

  local last_group = 'o'

  for _,info in ipairs(list) do
    -- determine the group
    local group

    if info.ed_group then
      group = info.ed_group
    elseif info.ed_num < 200 then
      group = 'o'  -- obstacle
    elseif info.ed_num < 300 then
      group = 'p'  -- pickup
    elseif info.ed_num < 400 then
      group = 'e'  -- enemy
    else
      group = 'm'  -- misc
    end

    if group ~= last_group then
      print()
      last_group = group
    end

    -- determine the radius
    -- [ it must be halved due to an oddity of our renderer ]
    local width = 4

    if info.tile_W then
      width = info.tile_W
    elseif info.image then
      width = info.image:getWidth()
    end

    width = width * info.scale

    width = math.ceil(width / 4)

    print(string.format("thing %d %s - %3d %s \"%s\"",
          info.ed_num, group, width, info.ed_sprite or "NULL",
          info.ed_name or info.name))
  end
end

